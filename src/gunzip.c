/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood 
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>

#include <libastrodb/db.h>
#include <libastrodb/library.h>
#include <libastrodb/gunzip.h>
#include <libastrodb/adbstdio.h>

#define FILE_LENGTH	1024

int gunzip_file(struct astrodb_db * cat, char *file)
{
	int pid = 0;
	int ret = 0;
	int fd[2];
	char src[FILE_LENGTH];
	int status;
	
	if ((ret = pipe(fd)) < 0)
		return ret;
	
	strncpy(src, cat->local_path, FILE_LENGTH);
	strncat(src, file, 1024 - strlen(src));
	astrodb_info("gunzip cmd %s\n", src);
	
	pid = vfork();
	if (pid == 0) {
		/* child */
		close(fd[0]);
		if (fd[1] != STDERR_FILENO)
			dup2(fd[1], STDERR_FILENO);
		ret = execlp("gunzip", "-f", "-v", "-d", src, NULL);
		if (ret < 0) {
			astrodb_error("failed to gunzip %s %d\n", src, ret);
			exit(ret);
		}
		
	} else {
		/* parent */
		close(fd[1]);
		if (fd[0] != STDIN_FILENO)
			dup2(fd[0], STDIN_FILENO);
		
		wait(&status);
	}
	if (WIFEXITED(status)) {
		if (WEXITSTATUS(status))
			return -WEXITSTATUS(status);
		else
			return 0;
	}
	
	/* gunzip failed */
	astrodb_error("gunzip fork failed\n");
	return -EIO;
}
