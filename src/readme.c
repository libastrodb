/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood  
 */

#define _GNU_SOURCE		/* needed for strndup */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>

#include <libastrodb/astrodb.h>
#include <libastrodb/readme.h>
#include <libastrodb/adbstdio.h>

#define README_LINE_SIZE    80 + 2

/*! \fn static int get_int (char* line, int pos, int line_size)
 * \brief Get int in string at pos
 */
static int get_int(char *line, int pos, int line_size)
{
	char *item_start = NULL;
	char *line_end = line + line_size;
	
	while (pos-- >= 0) {
		while (*line == ' ' && line < line_end)
			line++;
		
		item_start = line;
		while (*line != ' ' && line < line_end)
			line++;
	}
	return strtol(item_start, NULL, 10);
}

/*! \fn static char* get_string (char* line, int pos, int line_size, int all)
 * \brief Get space delimated string at position.
 */
static char *get_string(char *line, int pos, int line_size, int all)
{
	char *item_start = NULL;
	char *line_end = line + line_size;
	int item_size = 0;
	
	while (pos-- >= 0) {
		while (*line == ' ' && line < line_end)
			line++;
		
		item_size = 0;
		item_start = line;
		
		while (*line != ' ' && line < line_end) {
			line++;
			item_size++;    
		}
	}
	
	if (all)
		return strdup(item_start);
	else
		return strndup(item_start, item_size);
}

/*! \fn static void skip_lines (int lines, FILE* f)
 * \brief Read a line of text from F
 */
static void skip_lines(int lines, FILE *f)
{
	char line[README_LINE_SIZE];
	
	while (lines) {
		fgets(line, README_LINE_SIZE, f);
		lines--;
	}
}

/*! \fn static int find_header(char* header, int num, FILE* f, char* data)
 * \brief Find section header <num>
 */
static int find_header(char *header, int num, FILE *f, char *data)
{
	char line[README_LINE_SIZE];
	char *eof;
	int i = 0;
	
	rewind(f);
	do {
		eof = fgets(line, README_LINE_SIZE, f);
		
		if (!strncmp(header, line, strlen(header))) {
			if (i == num) {
				if (data) {
					strncpy(data, line + strlen(header) + 1, 80);
					*(data + strlen(data) - 1) = 0;	/* remove cr */
				}
				return 0;
			}
			i++;
		}
	} while (eof != NULL);
	
	return -1;
}

/*! \fn static int get_designation(astrodb_readme_info* info, FILE* f)
 * \brief Get catalog designation
 */
static int get_designation(struct astrodb_readme_info *info, FILE *f)
{
	info->designation = calloc(1, README_LINE_SIZE);
	if (info == NULL)
		return -ENOMEM;
	
	/* designation is always first line of ReadMe */
	rewind(f);
	fgets(info->designation, README_LINE_SIZE, f);
	return 0;
}

static int get_titles(struct astrodb_readme_info *info, FILE *f)
{
	
	return 0;
}

static int get_keywords(struct astrodb_readme_info *info, FILE *f)
{
	
	return 0;
}


static int get_description(struct astrodb_readme_info *info, FILE *f)
{
	
	return 0;
}

static int get_files(struct astrodb_readme_info *info, FILE *f)
{
	char line[README_LINE_SIZE];
	struct astrodb_table_info *file;
	int ret;
	
	ret = find_header("File Summary:", 0, f, NULL);
	if (ret < 0) {
		astrodb_error("failed to find File Summary\n");
		return -ENODATA;
	}
	
	/* read and skip in 3 lines of header */
	skip_lines(3, f);
	bzero(line, README_LINE_SIZE);
	fgets(line, README_LINE_SIZE, f);
	
	while (*line != '-') {
		file = calloc(1, sizeof(struct astrodb_table_info));
		if (file == NULL)
			return -ENOMEM;
		bzero(file, sizeof(struct astrodb_table_info));

		while (*line == ' ')
			fgets(line, README_LINE_SIZE, f);
		
		file->name = get_string(line, 0, 80, 0);
		if (file->name == NULL)
			goto err;

		file->records = get_int(line, 2, 80);
		file->length = get_int(line, 1, 80);
		file->title = get_string(line, 3, 80, 1);
		if (file->title == NULL)
			goto err;

		info->files = astrodb_dlist_append(info->files, (char *)file);
		fgets(line, README_LINE_SIZE, f);
	}
	info->files = astrodb_dlist_head(info->files);
	return 0;
err:
	free(file->name);
	free(file->title);
	free(file);
	return -ENOMEM;
}

/*! \fn static astrodb_dlist* get_byte_desc(FILE* f)
 * \brief Parse a byte by byte description
 */
static struct astrodb_dlist *get_byte_desc(FILE *f)
{
	char line[README_LINE_SIZE];
	char *line_adj = NULL;
	struct astrodb_table_column_info *desc = NULL;
	struct astrodb_dlist *byte_list = NULL;
	
	/* read in 3 line header */
	skip_lines(3, f);
	
	line[0] = 0;
	while (*line != '-') {
		desc = calloc(1, sizeof(struct astrodb_table_column_info));
		if (desc == NULL)
			return NULL;
		
		/* check for line continuation */
		do {
			fgets(line, README_LINE_SIZE, f);
			if (*line == '-') {
				free(desc);
				return byte_list;
			}
			desc->start = get_int(line, 0, 80);
			/* get the end, no space ! */
			desc->end = get_int(&line[5], 0, 75);
			
		} while (desc->start == 0 && desc->end == 0);
		
		/* subtract 1 from start and end to align */
		desc->start -= 1;
		desc->end -= 1;
		
		/* readjust ptr after data positions */
		line_adj = &line[9];
		desc->type = get_string(line_adj, 0, 80, 0);
		desc->units = get_string(line_adj, 1, 80, 0);
		desc->label = get_string(line_adj, 2, 80, 0);
		desc->explanation = get_string(line_adj, 3, 80, 1);
		byte_list = astrodb_dlist_append(byte_list, (char *) desc);
	}
	return byte_list;
}

/*! \fn static astrodb_table_info* get_file_data(astrodb_readme_info* info, char* names, int pos)
 * \brief Get a astrodb_table_info* from a file name 
 */
static struct astrodb_table_info *
get_file_data(struct astrodb_readme_info *info, char *names, int pos)
{
	char *name;
	struct astrodb_dlist *node = info->files;
	
	/* find ops entry */
	name = get_string(names, pos, 80, 0);
	if (strlen(name) == 0) {
		free(name);
		return NULL;
	}
	
	/* find pos file */
	while (node) {
		if (!strcmp(((struct astrodb_table_info *) node->data)->name, name)) {
			free(name);
			return (struct astrodb_table_info *) node->data;
		}
		node = node->tail;
	}
	
	free(name);
	return NULL;
}

static int get_byte_description(struct astrodb_readme_info * read_me_info, FILE * f)
{
	
	int i = 0, j = 0;
	char files[README_LINE_SIZE];
	struct astrodb_table_info *read_me_file;
	struct astrodb_dlist *desc;
	
	for (i = 0; !find_header("Byte-by-byte Description of file:", i, f, files); i++) {
		
		desc = get_byte_desc(f);
		desc = astrodb_dlist_head(desc);
		
		read_me_info->byte_desc = 
			astrodb_dlist_append(read_me_info->byte_desc, desc);
		
		for (j = 0; (read_me_file = get_file_data(read_me_info, files, j)) != NULL; j++) {
			read_me_file->byte_description = desc;
		}
	}
	
	for (i = 0; !find_header("Byte-per-byte Description of file:", i, f, files); i++) {
		
		desc = get_byte_desc(f);
		desc = astrodb_dlist_head(desc);
		
		read_me_info->byte_desc = 
			astrodb_dlist_append(read_me_info->byte_desc, desc);
		
		for (j = 0; (read_me_file = get_file_data(read_me_info, files, j)) != NULL; j++) {
			read_me_file->byte_description = desc;
		}
	}
	//#error match desc   	
	return 0;
}

struct astrodb_readme_info *parse_readme(char *file)
{
	struct astrodb_readme_info *info;
	FILE *f;
	
	f = fopen(file, "r");
	if (f == NULL) {
		astrodb_error("failed to open %s\n", file);
		return NULL;
	}
	
	info = (struct astrodb_readme_info *) 
		calloc(1, sizeof(struct astrodb_readme_info));
	if (info == NULL) {
		fclose(f);
		return NULL;
	}
	bzero(info, sizeof(struct astrodb_readme_info));
	
	get_designation(info, f);
	get_titles(info, f);
	get_keywords(info, f);
	get_description(info, f);
	get_files(info, f);
	get_byte_description(info, f);
	fclose(f);
	return info;
}

static void free_byte_desc(struct astrodb_table_column_info *b)
{
	free(b->type);
	free(b->units);
	free(b->label);
	free(b->explanation);
	free(b);
}

static void free_byte_descs(struct astrodb_dlist *list)
{
	list = astrodb_dlist_head(list);
	
	while (list) {
		struct astrodb_dlist *o;
		struct astrodb_dlist *s = list->data;
		while (s) {
			free_byte_desc(s->data);
			o = s;
			s = s->tail;
			free(o);
		}
		o = list;
		list = list->tail;
		free(o);
	}
}

static int free_file(void *data, void *user)
{
	struct astrodb_table_info *file = (struct astrodb_table_info *)data;
		
	free(file->name);
	free(file->title);
	return 0;
}

static int free_note(void *data, void *user)
{
	return 0;	
}

static int free_keyword(void *data, void *user)
{
	return 0;
}

void free_readme(struct astrodb_readme_info *info)
{
	struct astrodb_dlist *head;
	
	if (info->designation)
		free(info->designation);
	if (info->titles)
		free(info->titles);
	if (info->description)
		free(info->description);
	
	if ((head = info->files) != NULL)
		astrodb_dlist_foreach_free(head, free_file, NULL);
	
	if ((head = info->keywords) != NULL)
		astrodb_dlist_foreach_free(head, free_keyword, NULL);
	
	if ((head = info->notes) != NULL)
		astrodb_dlist_foreach_free(head, free_note, NULL);
	
	if (info->byte_desc)
		free_byte_descs(info->byte_desc);
	
	free(info);
}
