/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood 
 */

#define _GNU_SOURCE		/* for strtof and NAN */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>

#include <libastrodb/db.h>
#include <libastrodb/table.h>
#include <libastrodb/wget.h>
#include <libastrodb/gunzip.h>
#include <libastrodb/adbstdio.h>


#define FLOAT_SIZE '6'  /* CDS F string size and below for float else double */
#define FILE_LEN	1024

/*! \fn static int init_table(astrodb_table * table, int ra_stride, int dec_stride, int mag_stride)
 * \brief Initialise a dataset
 */
static int init_table(struct astrodb_table *table, 
			int ra_stride, int dec_stride, int mag_stride)
{
	struct astrodb_slist **slist, **hash_object;
	struct astrodb_tile_status *status;
	int i = 0;
	
	table->ra.stride = ra_stride;
	table->dec.stride = dec_stride;
	table->mag.stride = mag_stride;
	
	table->array_size = table->ra.stride * table->dec.stride * 
		table->mag.stride * sizeof(struct astrodb_slist *);
	table->status_size = table->ra.stride * table->dec.stride * 
		table->mag.stride * sizeof(struct astrodb_tile_status);
	table->no_tiles = table->ra.stride * table->dec.stride * 
		table->mag.stride;
	
	table->ra.stride_size = 
		fabs((table->db->ra_max - table->db->ra_min) / table->ra.stride);
	table->dec.stride_size = 
		fabs((table->db->dec_max - table->db->dec_min) / table->dec.stride);
	table->mag.stride_size = 
		fabs((table->db->mag_bright - table->db->mag_faint) / table->mag.stride);
	
	slist = (struct astrodb_slist **) calloc(1, table->array_size);
	if (slist == 0) {
		free(table);
		return -ENOMEM;
	}
	table->objects = slist;
	
	status = (struct astrodb_tile_status *) calloc(1, table->status_size);
	if (status == 0) {
		free(slist);
		free(table);
		return -ENOMEM;
	}
	table->status = status;
	
	for (i = 0; i < table->num_hmaps; i++) {
		hash_object = (struct astrodb_slist **) calloc(1, table->array_size);
		if (hash_object == NULL) {
			free(slist);
			free(status);
			while(i--)
				free(table->hmaps[i-1].hobjects);
			
			free(table);
			return -ENOMEM;
		}
		table->hmaps[i].hobjects = hash_object;
	}
	
	return 0;
}

/*! \fn static astrodb_table* create_table(astrodb_db*db, astrodb_dlist* desc, astrodb_dlist* files)
 * \brief Create a object dataset and populate fields
 */
static struct astrodb_table *create_table(struct astrodb_db *db, 
	struct astrodb_dlist *desc, struct astrodb_dlist *files)
{
	struct astrodb_table *table;
	
	table = calloc(1, sizeof(struct  astrodb_table));
	if (table == NULL)
		return NULL;
	
	bzero(table, sizeof(struct astrodb_table));
	table->byte_description = 
		((struct astrodb_table_info *) files->data)->byte_description;
	table->file = ((struct astrodb_table_info *) files->data)->name;
	table->table_size = ((struct astrodb_table_info *) files->data)->records;
	table->record_size = ((struct astrodb_table_info *) files->data)->length;
	table->db = db;
	db->num_tables++;
	return table;
}



/*! \fn astrodb_table *astrodb_table_create(astrodb_db *db, char *table_name, unsigned int flags)
 * \param db Catalog
 * \param table_name Dataset name (dataset file name in ReadMe)
 * \param flags Dataset creation flags.
 * \return A astrodb_table* for success or NULL on error
 *
 * Creates a dataset object.
 */
struct astrodb_table *astrodb_table_create(struct astrodb_db *db, 
			char *table_name, unsigned int flags)
{
	struct astrodb_dlist *files = db->info->files;
	struct astrodb_table *table = NULL;
	astrodb_info("Scanning for key fields in table: %s\n", table_name);
	
	while (files) {
		struct astrodb_table_info *table_info = 
			(struct  astrodb_table_info *) files->data;
		struct astrodb_dlist *byte_desc = table_info->byte_description;
		
		/* create a tile array per file for files that have a byte desc */
		if (byte_desc && 
			!strncmp(table_name, table_info->name, strlen(table_name))) {
			
			table = create_table(db, byte_desc, files);
			table->init_flags = flags;
			astrodb_info("  scan: %s\n", table_info->name);
			
			db->tables = astrodb_dlist_append(db->tables, 
								(char *) table);
			return table;
		}
		files = files->tail;
	}
	
	return NULL;
}

/*! \fn int astrodb_table_open(astrodb_table * table, astrodb_progress progress, int ra, int dec, int mag)
 * \param table dataset
 * \param progress Progress callback
 * \param ra RA stride
 * \param dec Dec stride
 * \param mag Magnitude stride
 *
 * Initialise a dataset. This will download and import the raw data if necessary.
 */
int astrodb_table_open(struct astrodb_table *table, int ra, int dec, int mag)
{
	struct stat stat_info;
	char file[FILE_LEN];
	char pstr[FILE_LEN];
	int ret;
	
	sprintf(pstr, "Initialising table %s", table->file);

	/* do we have to delete existing dataset */
	//if (flags & ADB_REFRESH)
	//	delete_table(astrodb_table* table);
	
	table_get_object_insert(table);
	
	ret = init_table(table, ra, dec, mag);
	if (ret < 0) {
		astrodb_error("failed to initialise table %d\n", ret);
		return ret;
	}

	/* check for local binary copy */
	sprintf(file, "%s%s%s", table->db->local_path, table->file, ".idx");
	ret = stat(file, &stat_info);
	if (ret == 0) {
		ret = table_load_hdr(table);
		if (ret < 0) {
			astrodb_error("failed to load binary header %s %d\n", 
				file, ret);
			return ret;
		}			
		ret = table_uncache_tiles(table);
		if (ret < 0)
			astrodb_error("failed to uncache table tiles %d\n", 
				ret);
		return ret;
	}

	/* check for local ASCII copy */
	sprintf(file, "%s%s", table->db->local_path, table->file);
	ret = stat(file, &stat_info);
	if (ret == 0)
		goto import;
	
	/* check for local */

	/* local binary or ASCII not available, so download ASCII */
	if (!(strncmp("http://", table->db->lib->remote, 7)) || 
		!(strncmp("ftp://", table->db->lib->remote, 6))) {
		ret = table_get_remote_dataset(table);
		if (ret < 0) {
			astrodb_error("failed to get remote table %d\n", ret);
			return ret;
		}
	}
import:
	if (table->init_flags & ADB_MEM) {
		ret = table_import(table, file);
		if (ret < 0) {
			astrodb_error("failed to import table %s %d\n", file, 
				ret);
			return ret;
		}
	}
	if (table->init_flags & ADB_FILE) {
		ret = table_save_hdr(table);
		if (ret < 0) {
			astrodb_error("failed to save table schema %d\n", ret);
			return ret;
		}
		ret = table_cache_tiles(table);
	}
	return ret;
}

/*! \fn void astrodb_table_close(astrodb_table* table)
 * \param table dataset
 *
 * Free dataset resources
 */
void astrodb_table_close(struct astrodb_table *table)
{
	int i, j;
	struct astrodb_db *db = table->db;
	
	/* free hash maps */
	for (i = 0; i < table->num_hmaps; i++) {
		for (j = 0; j < table->no_tiles; j++) {
			
			struct astrodb_slist *slist, *slist_;
			slist_ = *(table->hmaps[i].hobjects + j);
			
			while (slist_) {
				slist = slist_;
				slist_ = slist_->tail;
				free(slist);
			}
		}
		/* free array */
		free(table->hmaps[i].hobjects);
	}
	
	if (table->objects) {
		/* free objects in array */
		for (i = 0; i < table->no_tiles; i++)
			astrodb_slist_foreach_free(*(table->objects + i), NULL, 0);
		/* free array */
		free(table->objects);
	}
	
	if (table->status)
		free(table->status);
	
	db->tables = astrodb_dlist_free_object(db->tables, table);
}

/*! \fn void astrodb_table_put_objects(astrodb_slist *results)
 * \param results Results from astrodb_table_get_objects()
 * 
 * Releases resources held by the results from astrodb_table_get_objects().
 */
void astrodb_table_put_objects(struct astrodb_slist *results)
{
	struct astrodb_slist *slist;
	
	while (results) {
		slist = results;
		results = results->tail;
		free(slist);
	}
}

/*! \fn int astrodb_table_alt_column(astrodb_table* table, char* field, char* alt, int flags)
 * \param table dataset
 * \param field Primary field
 * \param alt Alternative field
 * \param flags flags
 *
 * Set an alternative import field if the primary field is blank.
 */
int astrodb_table_alt_column(struct astrodb_table *table, char *field, 
				char *alt, int flags)
{
	int idx, err;
	
	if (table->alt_fields >= TABLE_MAX_AIDX) {
		astrodb_error("too many alt fields %s\n", field);
		return -EINVAL;
	}
	
	idx = table_find_field(table, field);
	if (idx < 0) {
		astrodb_error("field not found %s\n", field);
		return -EINVAL;
	}
	
	err = table_add_alt_field(table, alt, idx);
	if (err < 0) {
		astrodb_error("failed to add field %s at idx %d\n", field, idx);
		return -EINVAL;
	}
	
	return 0;
}

/*! \fn void astrodb_table_clip_on_position (astrodb_table* table, double min_ra, double min_dec, double max_ra, double max_dec, double faint_mag,double bright_mag)
 * \param table dataset
 * \param min_ra Min RA 
 * \param min_dec Min Dec 
 * \param max_ra Max RA 
 * \param max_dec Max Dec
 * \param faint_mag Faint Magnitude
 * \param bright_mag Bright Magnitude
 * 
 * Clips the dataset to the within the given position range. Note: Clipping 
 * boundaries are not exact and depend on the granularity of the 
 * dataset position strides.
 */
void astrodb_table_clip_on_position(struct astrodb_table *table, 
					double min_ra, double min_dec, 
					double max_ra, double max_dec, 
					double faint_mag, double bright_mag)
{
	/* check ra,deg & mag boundaries */
	if (min_ra < table->db->ra_min)
		min_ra = table->db->ra_min;
	if (max_ra > table->db->ra_max)
		max_ra = table->db->ra_max;
	if (min_dec < table->db->dec_min)
		min_dec = table->db->dec_min;
	if (max_dec > table->db->dec_max)
		max_dec = table->db->dec_max;
	if (faint_mag > table->db->mag_faint)
		faint_mag = table->db->mag_faint;
	if (bright_mag < table->db->mag_bright)
		bright_mag = table->db->mag_bright;
	
	/* normalise boundaries */
	min_ra -= table->db->ra_min;
	max_ra -= table->db->ra_min;
	min_dec -= table->db->dec_min;
	max_dec -= table->db->dec_min;
	faint_mag -= table->db->mag_bright;
	bright_mag -= table->db->mag_bright;
		
	table->fov = 0;
	
	/* convert to stride boundaries */
	table->ra.clip_min = floor(min_ra / table->ra.stride_size);
	table->ra.clip_max = floor(max_ra / table->ra.stride_size);
	table->dec.clip_min = floor(min_dec / table->dec.stride_size);
	table->dec.clip_max = floor(max_dec / table->dec.stride_size);
	
	table->mag.clip_max = floor(bright_mag / table->mag.stride_size);
	table->mag.clip_min = floor(faint_mag / table->mag.stride_size);
}

/*! \fn void astrodb_table_clip_on_distance (astrodb_table* table, double min_AU, double max_AU)
 * \param table dataset
 * \param min_AU Min AU
 * \param max_AU Max AU
 * 
 * Clips the dataset to the within the given distance range. Note: Clipping 
 * boundaries are not exact and depend on the granularity of the 
 * dataset distance stride.
 */
void astrodb_table_clip_on_distance(struct astrodb_table *table, 
					double min_AU, double max_AU)
{
	/* check AU boundaries */
	if (min_AU < table->db->ra_min)
		min_AU = table->db->ra_min;
	if (max_AU > table->db->ra_max)
		max_AU = table->db->ra_max;
	
	/* normalise boundaries */
	min_AU -= table->db->ra_min;
	max_AU -= table->db->ra_min;
	
	table->fov = 0;
	
	/* convert to stride boundaries */
	table->ra.clip_min = (int) min_AU / table->ra.stride_size;
	table->ra.clip_max = ((int) max_AU / table->ra.stride_size) + 1;
}

/*! \fn void astrodb_table_clip_on_fov (astrodb_table* table, double ra, double dec, double radius,
 double faint_mag, double bright_mag);
 * \brief Set dataset clipping area based on field of view
 * \ingroup dataset
 */
void astrodb_table_clip_on_fov (struct astrodb_table *table, 
				double ra, double dec, double fov,
			   	double faint_mag, double bright_mag)
{
	/* check boundaries */
	table->fov = fov;
	if (ra < table->db->ra_min)
		ra = table->db->ra_min;
	if (ra > table->db->ra_max)
		ra = table->db->ra_max;
	if (dec < table->db->dec_min)
		dec = table->db->dec_min;
	if (dec > table->db->dec_max)
		dec = table->db->dec_max;
	if (faint_mag > table->db->mag_faint)
		faint_mag = table->db->mag_faint;
	if (bright_mag < table->db->mag_bright)
		bright_mag = table->db->mag_bright;
	
	table->clip_centre_ra = ra;
	table->clip_centre_dec = dec;   
	
	/* normalise boundaries */
	faint_mag -= table->db->mag_bright;
	bright_mag -= table->db->mag_bright;
	
	/* convert to stride boundaries */
	table->mag.clip_max = (int) bright_mag / table->mag.stride_size;
	table->mag.clip_min = ((int) faint_mag / table->mag.stride_size) + 1;
}

/*! \fn void astrodb_table_unclip (astrodb_table* table)
 * \param table dataset
 *
 * Unclip the dataset to it's full boundaries
 */
void astrodb_table_unclip(struct astrodb_table *table)
{
	int i,j,k, offset;
	
	/* in stride */
	table->ra.clip_min = 0;
	table->dec.clip_min = 0;
	table->ra.clip_max = table->ra.stride - 1;
	table->dec.clip_max = table->dec.stride - 1;
	table->mag.clip_min = table->mag.stride - 1;
	table->mag.clip_max = 0;
	table->fov = 0;
	
	for (i = 0; i <= table->ra.clip_max; i++) {
		for (j = 0; j <= table->dec.clip_max; j++) {
			for (k = 0; k <= table->mag.clip_min; k++) {
				offset = table_calc_offset(table, i, j, k);
				(table->status + offset)->flags &= ~ADB_SUSED;
			}
		}
	}
}

/*! int astrodb_table_get_objects (astrodb_table* table, astrodb_progress progress, astrodb_slist **result, unsigned int src)
 * \param table dataset
 * \param progress Progress callback
 * \param results dataset objects
 * \param src Get objects flags
 * \return number of objects, or negative for failed
 *
 * Get objects from dataset based on clipping area. Results must be released
 * with astrodb_table_put_objects() after use.
 */
int astrodb_table_get_objects(struct astrodb_table *table, 
				struct astrodb_slist **result, 
				unsigned int src)
{
	if (table->fov) {
		switch (src) {
		case ADB_SMEM:
			return table_get_objects_memc(table, result);
		case ADB_SCACHE:
			return table_get_objects_cachec(table, result);
		case ADB_SLOCAL:
			return table_get_objects_rawc(table, result);
		case ADB_SONLINE:
			return table_get_objects_onlinec(table, result);
		default:
			return ADB_ENOGET;
		}
	} else {
		switch (src) {
		case ADB_SMEM:
			return table_get_objects_mem(table, result);
		case ADB_SCACHE:
			return table_get_objects_cache(table, result);
		case ADB_SLOCAL:
			return table_get_objects_raw(table, result);
		case ADB_SONLINE:
			return table_get_objects_online(table, result);
		default:
			return ADB_ENOGET;
		}
	}
}

/*! \fn int astrodb_table_prune(astrodb_table* table)
 * \param table dataset
 * \return bytes free'd
 *
 * Free's oldest unused object tiles.
 */
int astrodb_table_prune(struct astrodb_table *table)
{
	return 0;
}

/*! \fn int astrodb_table_get_size(astrodb_table* table)
 * \param table dataset
 * \return size in bytes
 *
 * Get dataset memory usage.
 */
int astrodb_table_get_size(struct astrodb_table *table)
{
	return table->object_size * table->db->db_size;
}

/*! \fn void astrodb_table_set_status_posn (astrodb_table* table, double min_ra, double min_dec, double max_ra, double max_dec, double faint_mag,double bright_mag, unsigned int status)
 * \param table dataset
 * \param min_ra Min RA
 * \param min_dec Min Dec
 * \param max_ra Max RA 
 * \param max_dec Max Dec 
 * \param faint_mag Faint Magnitude
 * \param bright_mag Bright Magnitude
 * \param status Object status
 *
 * Set the status of objects within a given position range.
 */
void astrodb_table_set_status_posn(struct astrodb_table *table, 
					double min_ra, double min_dec, 
					double max_ra, double max_dec, 
					double faint_mag, double bright_mag, 
					unsigned int status)
{
	int i, j, k;
	int clip_min_ra, clip_max_ra, clip_min_dec, clip_max_dec,
	clip_bright_mag, clip_faint_mag;
	
	clip_min_ra = table->ra.clip_min;
	clip_max_ra = table->ra.clip_max;
	clip_min_dec = table->dec.clip_min;
	clip_max_dec = table->dec.clip_max;
	clip_bright_mag = table->mag.clip_max;
	clip_faint_mag = table->mag.clip_min;
	
	astrodb_table_clip_on_position(table, min_ra, min_dec, 
					max_ra, max_dec, bright_mag, faint_mag);
	
	for (i = table->ra.clip_min; i < table->ra.clip_max; i++) {
		for (j = table->dec.clip_min; j < table->dec.clip_max; j++) {
			for (k = table->mag.clip_max; 
				k < table->mag.clip_min; k++) {
				
			}
		}
	}
}

/*! \fn void astrodb_table_set_status_dist (astrodb_table* table, double min_AU, double max_AU, unsigned int status)
 * \param table Dataset
 * \param min_AU Minimum distance in AU
 * \param max_AU Maximum distance in AU
 * \param status Status flags to set
 *
 * Set the status of objects within a given distance range.
 */
void astrodb_table_set_status_dist(struct astrodb_table *table, 
					double min_AU, double max_AU, 
					unsigned int status)
{
	int min_offset = (min_AU - table->db->ra_min) / table->ra.stride_size;
	int max_offset = (max_AU - table->db->ra_min) / table->ra.stride_size;
	
	for (; min_offset < max_offset; min_offset++) {
		((struct astrodb_tile_status*)
			(table->status + min_offset))->flags = status;
	}
}

/*! \fn astrodb_tile_status astrodb_table_get_status_posn (astrodb_table* table, double ra, double dec, double mag)
 * \param table Dataset
 * \param ra RA in degrees
 * \param dec DEC in dergrees
 * \param mag Magnitude
 *
 * Get the status of objects within a tile at a give (ra,dec,mag) offset.
 */
struct astrodb_tile_status
astrodb_table_get_status_posn(struct astrodb_table *table, 
				double ra, double dec, double mag)
{
	int x, y, z, offset;
	
	x = floor((ra - table->db->ra_min) / table->ra.stride_size);
	y = floor((dec - table->db->dec_min) / table->dec.stride_size);
	z = floor((mag - table->db->mag_bright) / table->mag.stride_size);
	offset = table_calc_offset(table, x, y, z);
	
	return *(table->status + offset);
}

/*! \fn astrodb_tile_status astrodb_table_get_status_dist (astrodb_table* table, double AU)
 * \param table Dataset
 * \param AU Distance in AU
 * \returns Status
 *
 * Get the status of objects within a tile at a give distance offset.
 */
struct astrodb_tile_status 
astrodb_table_get_status_dist(struct astrodb_table *table, double AU)
{
	int offset = floor((AU - table->db->ra_min) / table->ra.stride_size);
	return *(table->status + offset);
}

/*! \fn astrodb_ctype astrodb_table_get_column_type(astrodb_table* table, char* field)
 * \param table Dataset
 * \param field Dataset field name
 * \return C type
 *
 * Get the C type for a field within a dataset.
 */
astrodb_ctype astrodb_table_get_column_type(struct astrodb_table *table, 
						char *field)
{
	int i;
	
	for (i = 0; i < table->object_fields; i++) {
		if (!strcmp(table->idx[i].symbol, field))
			return table->idx[i].type;
	}

	return CT_NULL;
}

/*! \fn int astrodb_table_get_column_offset(astrodb_table* table, char* field)
 * \param table Dataset
 * \param field Dataset field name
 * \return Field offset in bytes
 *
 * Gets the offset in bytes for a field within a dataset.
 */
int astrodb_table_get_column_offset(struct astrodb_table *table, char *field)
{
	int i;
	
	/* check custom fields */
	for (i = 0; i < table->object_fields; i++) {
		if (!strcmp(table->idx[i].symbol, field)) {
			return table->idx[i].s_offset;
		}
	}
	
	astrodb_error("failed to find field %s\n", field);
	return -EINVAL;
}

/*! \fn int astrodb_table_for_search_results_do(astrodb_slist* res, void(*func)(void* object, void* data), void* data)
 * \param res Results from get_objects
 * \param func Function to call per object in res
 * \param data Pointer to pass to func
 * \return 0 for success, negative for error
 *
 * Calls func for every object in res.
 */

int astrodb_table_for_search_results_do(struct astrodb_slist *results, 
					void (*func) (void *object, void *data), 
					void *data)
{
	if (func == NULL)
		return -EINVAL;
	
	while (results) {
		struct astrodb_slist *slist = results->data;
		while (slist) {
			func(slist->data, data);
			slist = slist->tail;
		}
		results = results->tail;
	}

	return 0;
}


/*! \fn int astrodb_table_get_row_size(astrodb_table* table);
 */
int astrodb_table_get_row_size(struct astrodb_table *table)
{
	return table->object_size;
}


/*! \fn void* astrodb_table_get_object (astrodb_table* table, char* id, char* field); 
 * \param table dataset
 * \param id object id
 * \param field dataset field
 * \return object or NULL if not found
 *
 * Get an object based on it' ID
 */
void* astrodb_table_get_object (struct astrodb_table *table, char *id, 
				char *field)
{
	struct astrodb_slist *slist = NULL;
	int offset, i;
	
	for (i = 0; i < table->num_hmaps; i++) {
		if (!strncmp(field, table->hmaps[i].field, 
			strlen(table->hmaps[i].field))) {
			goto get_object;
		}
	}
	astrodb_error("failed to find field %s\n", field);
	return NULL;
	
get_object:	
	offset = table_calc_hash_str(id, strlen(id), table->no_tiles);
	slist = *(table->hmaps[i].hobjects + offset);
	
	while (slist) {
		char* object_id = slist->data + table->hmaps[i].offset;
		if (!strstr(object_id, id))
			return slist->data;
		slist = slist->tail;
	}
	
	astrodb_error("failed to find id %s\n", id);
	return NULL;
}

/*! \fn int astrodb_table_hash_key(astrodb_table* table, char* field)
 * \param table dataset
 * \param field Field to be hashed.
 * \return 0 on success
 *
 * Add a field to be hashed for fast lookups.
 */
int astrodb_table_hash_key(struct astrodb_table* table, char* key)
{	
	if (table->num_hmaps == HASH_MAPS) {
		astrodb_error("too many hashed keys %s\n", key);
		return -EINVAL;
	}
	
	table->hmaps[table->num_hmaps].offset = 
		astrodb_table_get_column_offset(table, key);
	if (table->hmaps[table->num_hmaps].offset < 0) {
		astrodb_error("invalid column offset %s\n", key);
		return -EINVAL;
	}
	
	strncpy(table->hmaps[table->num_hmaps].field, key, 8);
	table->num_hmaps++;
	return 0;
}

/*! \fn int astrodb_table_register_schema(astrodb_table* table, astrodb_schema_object* idx, int idx_size);
 * \param table dataset
 * \param idx Object field index
 * \param idx_size Number of fields in index
 * \return 0 on success
 *
 * Register a new custom object type
 */
int astrodb_table_register_schema(struct astrodb_table *table, 
					struct astrodb_schema_object *idx,
					int idx_size, int object_size)
{
	int n, i;

	for (i = 0, n = 0; i < idx_size; i++) {
		if (!table_add_custom_struct_field(table, &idx[i]))
			n++;		
	}
	table->object_size = object_size;
	
	return n;
}
