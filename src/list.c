/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood 
 */


#include <stdlib.h>
#include <libastrodb/astrodb.h>

/*! \defgroup list Lists
 *
 * Single and double linked lists
 */


/*! \fn astrodb_slist* astrodb_slist_prepend(astrodb_slist* slist, void* object)
 * \param slist slist
 * \param object object
 * \return list head
 *
 * Prepends an object to the list head
 */
struct astrodb_slist *astrodb_slist_prepend(struct astrodb_slist *slist, 
						void *object)
{
	struct astrodb_slist *list = malloc(sizeof(struct astrodb_slist));
	
	if (list == NULL)
		return NULL;

	list->data = object;
	list->tail = slist;
	return list;
}

/*! \fn astrodb_slist* astrodb_slist_append(astrodb_slist* slist, void* object)
 * \param slist slist
 * \param object object
 * \return list tail
 *
 * Appends an object to the list tail
 */
struct astrodb_slist *astrodb_slist_append(struct astrodb_slist *slist, 
						void *object)
{
	struct astrodb_slist *list = malloc(sizeof(struct astrodb_slist));
	
	if (list == NULL)
		return NULL;
	
	
	list->data = object;
	list->tail = NULL;
	
	if (slist) {
		while (slist->tail)
			slist = slist->tail;
		slist->tail = list;
	}
	
	return list;
}

/*! \fn void astrodb_slist_foreach(astrodb_slist* slist, astrodb_func func, void* user)
 * \param slist slist
 * \param func Function to be called
 * \param user User data
 *
 * Calls a function for every object in slist.
 */
void astrodb_slist_foreach(struct astrodb_slist *slist, astrodb_func func, 
				void *user_data)
{
	if (func == NULL)
		return;

	while (slist) {
		func(slist->data, user_data);
		slist = slist->tail;
	}
}

/*! \fn void astrodb_slist_foreach_free(astrodb_slist* slist, astrodb_func func, void* user)
 * \param slist slist
 * \param func Function to be called
 * \param user User data
 *
 * Calls a function for every object in slist and then frees object.
 */
void astrodb_slist_foreach_free(struct astrodb_slist *slist, 
				astrodb_func func, void *user_data)
{
	struct astrodb_slist *slist_;
	
	if (func) {
		while (slist) {
			func(slist->data, user_data);
			slist_ = slist;
			slist = slist->tail;
			free(slist_);
		}
	} else {
		while (slist) {
			slist_ = slist;
			slist = slist->tail;
			free(slist_->data);
			free(slist_);
		}
	}
}

/*! \fn astrodb_dlist* astrodb_dlist_prepend(astrodb_dlist* dlist, void* object)
 * \param dlist dlist
 * \param object object
 * \return list head
 *
 * Prepends an object to the list head
 */
struct astrodb_dlist *astrodb_dlist_prepend(struct astrodb_dlist *dlist, 
						void *object)
{
	struct astrodb_dlist *list = malloc(sizeof(struct astrodb_dlist));
	
	if (list == NULL)
		return NULL;
	
	list->data = object;
	list->tail = dlist;
	list->head = NULL;
	return list;
}

/*! \fn astrodb_dlist* astrodb_dlist_prepend(astrodb_dlist* dlist, void* object)
 * \param dlist dlist
 * \param object object
 * \return list head
 *
 * Appends an object to the list head
 */
struct astrodb_dlist *astrodb_dlist_append(struct astrodb_dlist *dlist, 
						void *object)
{
	struct astrodb_dlist *list = malloc(sizeof(struct astrodb_dlist));
    
	if (list == NULL)
		return NULL;
		
	list->data = object;
	list->tail = NULL;

    	if (dlist) {
        	while (dlist->tail)
            		dlist = dlist->tail;
        	dlist->tail = list;
	}

	list->head = dlist;
	return list;
}

/*! \fn astrodb_dlist* astrodb_dlist_head(astrodb_dlist* dlist)
 * \param dlist dlist
 * \return head
 *
 * Get the head of a dlist
 */
struct astrodb_dlist *astrodb_dlist_head(struct astrodb_dlist *dlist)
{
	while (dlist->head)
		dlist = dlist->head;
	return dlist;
}

/*! \fn astrodb_dlist* astrodb_dlist_free_object(astrodb_dlist* dlist, void* object)
 * \param dlist dlist
 * \param object object
 * \return dlist object before object
 *
 * Free a single object in dlist
 */
struct astrodb_dlist *astrodb_dlist_free_object(struct astrodb_dlist *dlist,
						void *object)
{
	struct astrodb_dlist *head, *tail;

	 /* find object */
	dlist = astrodb_dlist_head(dlist);
	while (dlist->data != object && dlist)
		dlist = dlist->tail;

	if (dlist == NULL)
		return NULL;

	head = dlist->head;
	tail = dlist->tail;

	if (head)
		head->tail = tail;
	if (tail)
		tail->head = head;

	free(dlist->data);
	free(dlist);
    
	if (head)
		return head;
	if (tail) /* LIAM - remove */
		return tail;
	return NULL;
}

/*! \fn void astrodb_dlist_foreach(astrodb_dlist* dlist , astrodb_func func, void* user)
 * \param dlist dlist
 * \param func Function to be called
 * \param user User data
 *
 * Call function for every object in dlist
 */
void astrodb_dlist_foreach(struct astrodb_dlist *dlist, astrodb_func func, 
				void *user_data)
{
	dlist = astrodb_dlist_head(dlist);
    
	while (dlist) {
		func(dlist->data, user_data);
		dlist = dlist->tail;
	}
}

/*! \fn void astrodb_dlist_foreach_free(astrodb_dlist* dlist, astrodb_func func, void* user)
 * \param dlist dlist
 * \param func Function to be called
 * \param user User data
 *
 * Call function for every object in dlist and free object
 */
void astrodb_dlist_foreach_free(struct astrodb_dlist *dlist, 
				astrodb_func func, void *user_data)
{
	struct astrodb_dlist *dlist_;

	dlist = astrodb_dlist_head(dlist);
	
	if (func) {
		while (dlist) {
			func(dlist->data, user_data);
			dlist_ = dlist;
			dlist = dlist->tail;
			free(dlist_->data);
			free(dlist_);
		}
	} else {
		while (dlist) {
			dlist_ = dlist;
			dlist = dlist->tail;
			free(dlist_->data);
			free(dlist_);
		}
	}
}
