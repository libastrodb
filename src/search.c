/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood 
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <libastrodb/search.h>
#include <libastrodb/object.h>
#include <libastrodb/adbstdio.h>

#define MAX_PARAMS	128
#define PARAM_OPER	0
#define PARAM_COMP	1
#define OP_OP	1
#define OP_COMP	2

/* comparators (type)_(oper)_comp */
static int int_lt_comp(void *data, void *value)
{
	return *(int *) data < *(int *) value;
}
static int int_gt_comp(void *data, void *value)
{
	return *(int *) data > *(int *) value;
}
static int int_eq_comp(void *data, void *value)
{
	return *(int *) data == *(int *) value;
}
static int int_ne_comp(void *data, void *value)
{
	return *(int *) data != *(int *) value;
}
static int float_lt_comp(void *data, void *value)
{
	return *(float *) data < *(float *) value;
}
static int float_gt_comp(void *data, void *value)
{
	return *(float *) data > *(float *) value;
}
static int float_eq_comp(void *data, void *value)
{
	return *(float *) data == *(float *) value;
}
static int float_ne_comp(void *data, void *value)
{
	return *(float *) data != *(float *) value;
}
static int double_lt_comp(void *data, void *value)
{
	return *(double *) data < *(double *) value;
}
static int double_gt_comp(void *data, void *value)
{
	return *(double *) data > *(double *) value;
}
static int double_eq_comp(void *data, void *value)
{
	return *(double *) data == *(double *) value;
}
static int double_ne_comp(void *data, void *value)
{
	return *(double *) data != *(double *) value;
}
static int string_lt_comp(void *data, void *value)
{
	return (strcmp(data, value) > 0);
}
static int string_gt_comp(void *data, void *value)
{
	return (strcmp(data, value) < 0);
}
static int string_eq_comp(void *data, void *value)
{
	return !strcmp(data, value);
}
static int string_ne_comp(void *data, void *value)
{
	return strcmp(data, value);
}
static int string_eq_wildcard_comp(void *data, void *value)
{
	int i = 0;
	char *ptr = value;
	
	while (*ptr != '*') {
		i++;
		ptr++;
	}
	
	return !strncmp(data, value, i);
}

/* comparator list operators */
static int comp_AND(void *object, struct astrodb_slist *slist)
{
	if (slist->tail)
		return ((struct search_node *) slist->data)->comp(object + 
				((struct search_node *) slist->data)->offset, 
				((struct search_node *) slist->data)->value) 
				&& comp_AND(object, slist->tail);
	return ((struct search_node *) slist->data)->comp(object + 
		((struct search_node *) slist->data)->offset,
		((struct search_node *) slist->data)->value);
}

static int comp_OR(void *object, struct astrodb_slist *slist)
{
	if (slist->tail)
		return ((struct search_node *) slist->data)->comp(object +
			((struct search_node *) slist->data)->offset,
			((struct search_node *) slist->data)->value)
			|| comp_OR(object, slist->tail);
	return ((struct search_node *) slist->data)->comp(object +
			((struct search_node *) slist->data)->offset,
			((struct search_node *) slist->data)->value);
}

/*  node list operators, operate on list of struct search_branch's */
static int node_AND_comp(void *object, struct astrodb_slist * slist)
{
	struct search_node *node = (struct search_node *) slist->data;
	
	if (slist->tail)
		return node->comp(object + node->offset, node->value) && 
			node_AND_comp(object, slist->tail);
	return node->comp(object + node->offset, node->value);
}

static int node_OR_comp(void *object, struct astrodb_slist * slist)
{
	struct search_node *node = (struct search_node *) slist->data;
	
	if (slist->tail)
		return node->comp(object + node->offset, node->value) || 
			node_OR_comp(object, slist->tail);
	return node->comp(object + node->offset, node->value);
}

/*  node list operators, operate on list of struct search_branch's */
static int node_AND_oper(void *object, struct astrodb_slist *slist)
{
	struct search_branch *node = (struct search_branch *) slist->data;
	
	if (slist->tail)
		return node->oper(object, node->slist) && 
			node_AND_oper(object, slist->tail);
	return node->oper(object, node->slist);
}

static int node_OR_oper(void *object, struct astrodb_slist * slist)
{
	struct search_branch *node = (struct search_branch *) slist->data;
	
	if (slist->tail)
		return node->oper(object, node->slist) || 
			node_OR_oper(object, slist->tail);
	return node->oper(object, node->slist);
}


static comp_ get_comparator(enum astrodb_comparator comp, astrodb_ctype ctype)
{
	switch (ctype) {
	case CT_INT:
		switch (comp) {
		case ADB_COMP_LT:
			return int_lt_comp;
		case ADB_COMP_GT:
			return int_gt_comp;
		case ADB_COMP_EQ:
			return int_eq_comp;
		case ADB_COMP_NE:
			return int_ne_comp;
		}
		break;
	case CT_FLOAT:
		switch (comp) {
		case ADB_COMP_LT:
			return float_lt_comp;
		case ADB_COMP_GT:
			return float_gt_comp;
		case ADB_COMP_EQ:
			return float_eq_comp;
		case ADB_COMP_NE:
			return float_ne_comp;
		}
		break;
	case CT_DOUBLE:
		switch (comp) {
		case ADB_COMP_LT:
			return double_lt_comp;
		case ADB_COMP_GT:
			return double_gt_comp;
		case ADB_COMP_EQ:
			return double_eq_comp;
		case ADB_COMP_NE:
			return double_ne_comp;
		}
		break;
	case CT_STRING:
		switch (comp) {
		case ADB_COMP_LT:
			return string_lt_comp;
		case ADB_COMP_GT:
			return string_gt_comp;
		case ADB_COMP_EQ:
			return string_eq_comp;
		case ADB_COMP_NE:
			return string_ne_comp;
		}
		break;
	case CT_DOUBLE_DMS_DEGS:
	case CT_DOUBLE_DMS_MINS:
	case CT_DOUBLE_DMS_SECS:
	case CT_DOUBLE_HMS_HRS:
	case CT_DOUBLE_HMS_MINS:
	case CT_DOUBLE_HMS_SECS:
	case CT_SIGN:
	case CT_NULL:
	case CT_DOUBLE_MPC:
		return NULL;
	}
	return NULL;
}

/*! \fn astrodb_search* astrodb_search_create(astrodb_table *table)
 * \param table dataset
 * \returns astrodb_search object on success or NULL on failure
 *
 * Creates an new search object
 */
struct astrodb_search *astrodb_search_create(struct astrodb_table *table)
{
	struct astrodb_search *srch = 
		(struct astrodb_search *) 
			calloc(1, sizeof(struct astrodb_search));
	if (srch == NULL)
		return NULL;
	
	srch->table = table;
	return srch;
}

static int free_orphans(void *data, void *user)
{
	struct astrodb_slist *slist = data;
	
	while (slist) {
		struct astrodb_slist *tmp = slist;
		slist = slist->tail;
		free(tmp);
	}
	return 0;
}

/*! \fn void astrodb_search_free(astrodb_search* search);
 * \param search Search
 *
 * Free's a search and it resources
 */
void astrodb_search_free(struct astrodb_search *search)
{
	astrodb_slist_foreach_free(search->free_list, NULL, NULL);
	astrodb_slist_foreach_free(search->free_slist, free_orphans, NULL);
	free(search);
}

/*! \fn int astrodb_search_add_operator(astrodb_search* search, astrodb_operator op);
 * \param search Search
 * \param op Operator
 * \returns 0 on success
 *
 * Add a node operation in RPN to the search
 */
int astrodb_search_add_operator(struct astrodb_search * search, 
				enum astrodb_operator op)
{
	struct search_branch *branch;
	
	branch = calloc(1, sizeof(struct search_branch));
	if (branch == NULL)
		return -ENOMEM;
	
	/* cannnot have lone operator */
	if (search->root == NULL && 
		search->oper_orphans == NULL && 
		search->comp_orphans == NULL) {
		free(branch);
		return -EINVAL;
	}
	
	/* we either have a parent of a comp or op
	 * or a sibling of an op 
	 *
	 * comparator parent = comp_orphans != NULL, oper_orphans = NULL
	 * operator parent = comp_orphans = NULL, oper_orphans != NULL
	 */
	
	if (search->comp_orphans != NULL) {
		/* comparator parent */
		switch (op) {
		case ADB_OP_AND:
			branch->oper = node_AND_comp;
			break;
		case ADB_OP_OR:
			branch->oper = node_OR_comp;
			break;
		}
		branch->type = OP_COMP;
		branch->slist = search->comp_orphans;
		search->free_slist = 
			astrodb_slist_prepend(search->free_slist, 
						search->comp_orphans);
		search->comp_orphans = NULL;
	} else {
		/* operator parent */
		switch (op) {
		case ADB_OP_AND:
			branch->oper = node_AND_oper;
			break;
		case ADB_OP_OR:
			branch->oper = node_OR_oper;
			break;
		}
		branch->slist = search->oper_orphans;
		branch->type = OP_OP;
		search->free_slist = 
			astrodb_slist_prepend(search->free_slist, 
						search->oper_orphans);
		search->oper_orphans = NULL;
	}
	
	/* we are an orphan ourselves */
	search->oper_orphans = 
		astrodb_slist_prepend(search->oper_orphans, branch);
	search->free_list = astrodb_slist_prepend(search->free_list, branch);
	search->start_oper = branch;
	search->num_search_nodes++;

	return 0;
}

/*! \fn int astrodb_search_add_comparator(astrodb_search* search, char* field, astrodb_comparator comp, char* value);
 * \param search Search
 * \param field Field name
 * \param comp Comparator
 * \param value Compare value
 *
 * Add a comparator in RPN to the search
 */
int astrodb_search_add_comparator(struct astrodb_search *search, char *field, 
				enum astrodb_comparator comp, char *value)
{
	astrodb_ctype ctype;
	struct search_node *node;
	comp_ srch_comp;
	
	ctype = astrodb_table_get_column_type(search->table, field);
	srch_comp = get_comparator(comp, ctype);
	if (srch_comp == NULL) {
		astrodb_error("failed to object_slist comparator %d for Ctype %d\n", 
			comp, ctype);
		return -EINVAL;
	}
	if (strstr(value, "*"))
		srch_comp = string_eq_wildcard_comp;
	
	node = calloc(1, sizeof(struct search_node));
	if (node == NULL)
		return -ENOMEM;
	
	node->offset = astrodb_table_get_column_offset(search->table, field);
	node->comp = srch_comp;
	
	switch (ctype) {
	case CT_SIGN:
	case CT_NULL:
	case CT_STRING:
	case CT_DOUBLE_MPC:
		node->value = strdup(value);
		break;
	case CT_INT:
		node->value = calloc(1, sizeof(int));
		*((int *) node->value) = strtol(value, NULL, 10);
		break;
	case CT_FLOAT:
		node->value = calloc(1, sizeof(float));
		*((float *) node->value) = strtod(value, NULL);
		break;
	case CT_DOUBLE:
	case CT_DOUBLE_DMS_DEGS:
	case CT_DOUBLE_DMS_MINS:
	case CT_DOUBLE_DMS_SECS:
	case CT_DOUBLE_HMS_HRS:
	case CT_DOUBLE_HMS_MINS:
	case CT_DOUBLE_HMS_SECS:
		node->value = calloc(1, sizeof(double));
		*((double *) node->value) = strtod(value, NULL);
		break;
	}
	search->comp_orphans = 
		astrodb_slist_prepend(search->comp_orphans, node);
	search->free_list = astrodb_slist_prepend(search->free_list, node);
	search->free_list = 
		astrodb_slist_prepend(search->free_list, node->value);
	search->num_search_nodes++;
	search->start_oper = NULL;
	
	return 0;
}

/*! \fn int astrodb_search_add_custom_comparator(astrodb_search* search, astrodb_custom_comparator comp)
 * \param search Search
 * \param comp Custom comparator function
 * \returns 0 on success
 *
 * Add a custom search comparator. It should return 1 for a match and 0 for a
 * search miss.
 */
int astrodb_search_add_custom_comparator(struct astrodb_search *search, 
					astrodb_custom_comparator comp)
{
	return -EINVAL;
}

/*! \fn int astrodb_search_get_results(astrodb_search* search, astrodb_progress progress, astrodb_slist **result, unsigned int src) 
 * \param search Search
 * \param progress Progress callback
 * \param result Search results
 * \param src Object source
 *
 * Get search results.
 */
int astrodb_search_get_results(struct astrodb_search *search, 
				struct astrodb_slist **result, 
				unsigned int src)
{
	struct astrodb_slist *object_slist = NULL, *object_slist_;
	struct astrodb_slist *res = NULL;
	*result = NULL;
	
	if (search->oper_orphans) {
		search->root = search->oper_orphans;
		search->free_slist = 
			astrodb_slist_prepend(search->free_slist, 
							search->oper_orphans);
		search->oper_orphans = NULL;
	} else if (search->comp_orphans) {
		astrodb_error("unbalanced search - comp orphans exist\n");
		return -EINVAL;
	}
	
	if (search->start_oper == NULL) {
		astrodb_error("unbalanced search - no start oper\n");
		return -EINVAL;
	}
	
	/* operator is root */
	astrodb_table_get_objects(search->table, &object_slist, src);
	object_slist_ = object_slist;
	while (object_slist_) {
		struct astrodb_slist *object = object_slist_->data;
		struct astrodb_slist *slist = 
			((struct search_branch *) search->root->data)->slist;
		
		while (object) {
			if (search->start_oper->oper(object->data, slist)) {
				search->hits++;
				res = astrodb_slist_prepend(res, object->data);
			}
			search->tests++;
			object = object->tail;
		}
		object_slist_ = object_slist_->tail;
	}
		
	
	astrodb_table_put_objects(object_slist);
	*result = astrodb_slist_prepend(*result, res);
	return 0;
}

/*! \fn void astrodb_search_put_results (astrodb_slist *results)
 * \param results
 *
 * Frees search results
 */
void astrodb_search_put_results(struct astrodb_slist *results)
{
	struct astrodb_slist *slist;
	
	if (results == NULL)
		return;
	
	slist = results->data;
	
	while (slist) {
		struct astrodb_slist *tmp = slist;
		slist = slist->tail;
		free(tmp);
	}
	free(results);
}

/*! \fn int astrodb_search_get_hits(astrodb_search* search);
 * \param search Search
 * \returns Hits
 *
 * Get the number of search hits.
 */
int astrodb_search_get_hits(struct astrodb_search *search)
{
	return search->hits;
}


/*! \fn int astrodb_search_get_tests(astrodb_search* search);
 * \param search Search
 * \returns Tests
 *
 * Get the number of search tests
 */
int astrodb_search_get_tests(struct astrodb_search *search)
{
	return search->tests;
}
