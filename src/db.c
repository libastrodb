/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood  
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>

#include <libastrodb/db.h>
#include <libastrodb/library.h>
#include <libastrodb/object.h>
#include <libastrodb/readme.h>
#include <libastrodb/wget.h>
#include <libastrodb/adbstdio.h>

#define STRLEN_HTTP	7
#define STRLEN_FTP	6
#define FILE_LEN	1024

static int load_readme(struct astrodb_db *db, unsigned int flags)
{
	int ret = 0;
	struct stat buf;
	char file[FILE_LEN];
	
	/* check for local binary copy */
	//    strncpy(file, db->local_path, 1024);
	//    strncat(file, "db.ncd", 1024 - strlen(file));
	//    if (stat(file, &buf) == 0){
	//        //load_catalog(db, file);
	//        return 0;
	//   }
	
	/* check for local disk ReadMe (ASCII) copy */
	strncpy(file, db->local_path, FILE_LEN);
	strncat(file, "ReadMe", FILE_LEN - strlen(file));
	if (stat(file, &buf) == 0)
		goto parse;
	
	/* local binary or ASCII not available, so download ASCII ReadMe*/
	if (!(strncmp("http://", db->lib->remote, STRLEN_HTTP)) || 
		!(strncmp("ftp://", db->lib->remote, STRLEN_FTP))) {
		
		ret = wget_file(db, "ReadMe");
		if (ret < 0) {
			astrodb_error("failed to load ReadMe %d\n", ret);
			return ret;
		}
	}
parse:
	db->info = parse_readme(file);
	if (db->info == NULL) {
		astrodb_error("failed to parse ReadMe\n");
	      	return -EINVAL;
	}

	return 0;
}

/*! \fn astrodb_db* astrodb_create_db(struct astrodb_library* lib, 
 * 					char* cclass, char* cnum, 
 * 					double ra_min, double ra_max,
 * 					double dec_min, double dec_max, 
 * 					double mag_faint, double mag_bright, 
 * 					int flags)
 * \param lib Library
 * \param cclass Catalog class
 * \param cnum Catalog number
 * \param ra_min Minimum RA boundary
 * \param ra_max Maximum RA boundary
 * \param dec_min Minimum Dec boundary
 * \param dec_max Maximum Dec boundary
 * \param mag_faint Faint magnitude boundary
 * \param mag_bright Bright magnitude boundary
 * \param flags catalog create flags
 *
 * Initialise a Catalog by parsing it's ReadMe. The catalog may be limited
 * in sky area and magnitude by using the boundary parameters.
 */
struct astrodb_db *astrodb_create_db(struct astrodb_library *lib, 
				char *cat_class, char *cat_num, 
				double ra_min, double ra_max, 
				double dec_min, double dec_max, 
				double mag_faint, double mag_bright, int flags)
{
	struct astrodb_db *db = NULL;
	char local[FILE_LEN];
	char remote[FILE_LEN];
	struct stat stat_info;
	int err;
	
	db = (struct astrodb_db *) malloc(sizeof(struct astrodb_db));
	if (db == NULL)
		return NULL;
	
	bzero(db, sizeof(struct astrodb_db));
	db->lib = lib;
	
	/* set up bounds */
	db->ra_min = ra_min;
	db->ra_max = ra_max;
	db->dec_min = dec_min;
	db->dec_max = dec_max;
	db->mag_faint = mag_faint;
	db->mag_bright = mag_bright;
	
	/* setup the paths */
	db->cat_class = strdup(cat_class);
	if (db->cat_class == NULL)
		goto err;
	db->cat_num = strdup(cat_num);
	if (db->cat_num == NULL)
		goto err;
	sprintf(remote, "%s%s%s%s%s%s", db->lib->remote, "/", 
		cat_class, "/", cat_num, "/");
	sprintf(local, "%s%s%s%s%s%s", db->lib->local, "/", 
		cat_class, "/", cat_num, "/");
	db->remote_path = strdup(remote);
	if (db->remote_path == NULL)
		goto err;
	db->local_path = strdup(local);
	if (db->local_path == NULL)
		goto err;
	
	err = stat(db->local_path, &stat_info);
	if (err < 0) {
		err = mkdir(db->local_path, S_IRWXU | S_IRWXG);
		if (err < 0) {
			astrodb_error("failed to create dir %s %d\n", 
					db->local_path, err);
			goto err;
		}
	}
	
	/* try and retrieve catalog info */
	err = load_readme(db, flags);
	if (err < 0)
		goto err;
	
	return db;
err:
	astrodb_error("failed to create db %d\n", err);
	free(db->remote_path);
	free(db->local_path);
	free(db->cat_class);
	free(db->cat_num);
	free(db);
	return NULL;
}

/*! \fn astrodb_dlist* astrodb_db_get_tables(astrodb_db *db)
 * \param db Catalog
 * \return astrodb_dlist of astrodb_table_info
 * 
 * Query the datasets within the catalog
 */
struct astrodb_dlist *astrodb_db_get_tables(struct astrodb_db *db)
{
	if (db->info == NULL)
		return NULL;
	return db->info->files;
}

/*! \fn void astrodb_db_free (astrodb_db *db)
 * \param db Catalog
 * 
 * Free's all catalog resources
 */
void astrodb_db_free(struct astrodb_db *db)
{
	free(db->cat_class);
	free(db->cat_num);
	free(db->local_path);
	free(db->remote_path);
	free_readme(db->info);
	free(db);
}

/*! \fn const char* astrodb_get_version(void);
 * \return libastrodb version
 *
 * Get the libastrodb version number.
 */
const char *astrodb_get_version(void)
{
	return VERSION;
}

struct _error {
	const char* lncerr;
	unsigned int lncerrno;
};

struct _error error[] = {
	{"ok", ADB_EOK},
	{"not implemented", ADB_EIMPL},
	{"invalid dataset", ADB_EIDSET},
	{"invalid catalog", ADB_EICAT},
	{"invalid CDS ReadMe", ADB_EIREADME},
	{"no such file or directory", ADB_ENOFILE},
	{"no memory", ADB_ENOMEM},
	{"could not create catalog tree", ADB_ENOCATDIR},
	{"no valid object insert", ADB_ENOINS},
	{"field not valid or found", ADB_ENOFIELD},
	{"no such function", ADB_ENOFUNC},
	{"external command failed", ADB_ECMD},
	{"invalid search", ADB_ESRCH},
	{"invalid search operator", ADB_ENOOP},
	{"invalid search comparator", ADB_ENOCOMP},
	{"invalid object get", ADB_ENOGET},
	{"invalid parameter", ADB_EINV},
};

/*! \fn const char* astrodb_get_last_err_msg();
 * \return libastrodb last error message
 *
 * Get the text of the last error.
 */
const char *astrodb_get_err_strg(int err)
{
	int i;
	
	for (i = 0; i< astrodb_size(error); i++) {
		if (err == error[i].lncerrno)
			return error[i].lncerr;
	}
	
	return "Unknown error";
}


