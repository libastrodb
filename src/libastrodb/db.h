/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2005 Liam Girdwood 
 */

#ifndef __ADB_DB_H
#define __ADB_DB_H

#include <stdio.h>

#include <libastrodb/astrodb.h>
#include <libastrodb/readme.h>
#include <libastrodb/library.h>

/* catalog load flags */
#define ADB_REFRESH_CACHE	0x10	/*!< Refresh cache from local copy */
#define ADB_REFRESH_LOCAL	0x20	/*!< Refresh local copy from master (online) catalog */

/* magic numbers */
#define ADB_CAT_MAGIC		0x0  /*! LNC0 */

/*! \defgroup catalog Catalog
 *
 * CDS Catalog. This object represents a single CDS catalog.
 */

/*! \defgroup misc Miscellaneous
 *
 * Miscellaneous functions.
 */

/*! \typedef struct astrodb_db
 *
 * This struct is a container for any single type of astronomical objects.
 *
 * It stores all deep sky objects in a 3 dimensional array based on ra*dec*mag
 * for clipping and quick searching. It also allow catalogs to be split 
 * into several files.
 *
 * Near sky objects are stored in a virtual 1 dimensional array depending on 
 * there semi-major axis.
 */

struct astrodb_db {
	double ra_min;			/*!< Min catalog RA or AU */
	double ra_max;			/*!< Max catalog RA or AU */
	double dec_min;			/*!< Min catalog DEC */
	double dec_max;			/*!< Max catalog DEC */
	double mag_faint;		/*!< Min (faint) catalog mag */
	double mag_bright;		/*!< Mag (bright) catalog mag */
	long private;			/*!< private, for use by user */
	int db_size;			/*!< Number of objects in catalog */
	char* cat_class;		/*!< catalog class */
	char* cat_num;			/*!< catalog number (in repo) */
	char* local_path;   		/*!< local catalog path */
	char* remote_path;  		/*!< remote catalog path */
	struct astrodb_library *lib;	/*!< catalog parent library n:1 */
	struct astrodb_readme_info *info;/*!< Catalog ReadMe data 1:1 */
	int num_tables;      		/*!< Number of catalog data sets */
	struct astrodb_dlist *tables;   /*!< Catalog datasets */
};

#endif
