/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2005 Liam Girdwood  
 */

#ifndef __LNC_LIBARY_H
#define __LNC_LIBARY_H
	
#include <libastrodb/astrodb.h>


/*! \defgroup library Library
 *
 * An object representing a local catalog repository.
 *
 * A local library is a mirror of a remote CDS mirror in 
 * structure. Catalogs can then be downloaded in part or in whole
 * on a need by need basis to populate the library.
 *
 * CDS directory structure mirrored:-
 *
 *  - I/number  		Astrometric Catalogues
 *  - II/number 		Photometric Catalogues (except Radio)
 *  - III/number	 	Spectroscopic Catalogues
 *  - IV/number 		Cross-Identifications
 *  - V/number 			Combined Data
 *  - VI/number 		Miscellaneous Catalogues
 *  - VII/number 		Non-stellar Objects
 *  - VIII/number 		Radio Catalogues
 *  - IX/number 		High Energy Catalogues
 */
 
/*! \typedef struct astrodb_library
 * \brief Local CDS catalog repository
 * \ingroup library
 * 
 * The library container.
 */
struct astrodb_library {
	char* local;	/*!< local repository and cache */
	char* remote;	/*!< remote repository */
	unsigned int err; /*!< last error */
};

#endif
