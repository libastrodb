/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood  
 */

#ifndef __LNC_SEARCH_H
#define __LNC_SEARCH_H

#include <libastrodb/astrodb.h>
#include <libastrodb/db.h>
#include <libastrodb/table.h>

/*! \defgroup search Search
 *
 * Searching
 *
 * Reverse Polish Notation is used to define the search parameters. 
 */

/* compares <data> with <value> using <comparator> */
typedef int (*comp_)(void *data, void *value);

/* operation on  nodes / lists */
typedef int (*oper_)(void *object, struct astrodb_slist *slistl);

struct search_node {
	comp_ comp;		/*!< comparator func */
	int offset;		/*!< data offset */
	void *value;		/*!< value ptr */
};

struct search_branch {
	struct astrodb_slist *slist;	/*!< list of child comparators or search_branch's */
	oper_ oper;		/*!< operator on list */
	unsigned int type;	/*!< operator type, op-op OR op-comp */
};

/*! \typedef struct astrodb_search
 * \ingroup search
 * \brief Search object
 */
struct astrodb_search {
   	int tests;         		/*!< number of search tests */
   	int hits;			/*!< number of search hits */
	struct astrodb_table *table;	/*!< table that is searched */
	struct astrodb_slist *root;	/*!< root search node */
	struct astrodb_slist *oper_orphans;	/*!< orphaned operator children */
	struct astrodb_slist *comp_orphans;/*!< orphaned comparator children */
	struct search_branch *start_oper;
	int num_search_nodes;		/*!< number of search nodes */
	struct astrodb_slist *free_list;/*!< data for freeing */
	struct astrodb_slist *free_slist;/*!< slist's for freeing */
};

#endif
