/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood 
 */

#ifndef __LNC_DSET_H
#define __LNC_DSET_H

#include <stdlib.h>

#include <libastrodb/db.h>

#define TABLE_MAX_IDX		128	/* max number of indexes */
#define TABLE_MAX_AIDX		16	/* max number of alternate indexes */
#define HASH_MAPS		16	/* number of hash maps */

#define INSERT_POSN_MAG		1
#define INSERT_POSN_TYPE	2
#define INSERT_HASH_ID		3
#define INSERT_POSN_MAG_HASH    4
#define INSERT_POSN_TYPE_HASH   5

	
/*! \defgroup dataset Dataset
*
* A dataset is a container of astro objects organised in either a 3,2 or 1 dimensional
* tile array. Each tile in the array contains a singly linked list of objects and
* status information about the tile.
*
* The dataset tiles can be organised as follows:-
*	- 3D as RA, DEC and Vmag
*	- 2D as RA, DEC
*	- 1D as hashed object ID
*	- 1D on object distance (for near object datasets)
*
* A dataset is generally equivalent to a table within a traditional database
* with additional data ordering.
*
* Datasets have a 1:1 mapping with with catalog data files and a catalog
* can have more than 1 dataset.
*/	

typedef int (*astrodb_alt_insert)(void *dest, void *src1, void *src2);

struct alt_idx_ {
	struct astrodb_schema_object pri;     /* primary field index */
	struct astrodb_schema_object sec;     /* alternate field index */
	astrodb_alt_insert insert;  /*!< alt field inserters */
};

struct hash_map {
	struct astrodb_slist **hobjects;   /*!< Hash table pointing to objects */
	int offset;		/*!< offset in object for hashed ID */
	char field[12];
};

struct dimension {
	int stride;		/*!< number of strides */
	double stride_size; 	/*!< stride size in degrees */
	int clip_min;
	int clip_max;
};

/*! \typedef struct struct astrodb_table
 * \brief Catalog Dataset.
 * \ingroup dataset
 * 
 * The dataset container holds the tile array and it's dimensions.
 */
struct astrodb_table {
	char *name;			/*!< dataset name */
	char *file;         		/*!< dataset local filename */
	struct astrodb_db *db;   	/*!< catalog we belong to */
	int init_flags;			/*!< initialisation flags */
	
	struct dimension ra;
	struct dimension dec;
	struct dimension mag;

	double fov;		/*!< Clipping radius in degrees */
	double clip_centre_ra;		/*!< Clipping centre RA (circular) */
	double clip_centre_dec;		/*!< Clipping centre DEC (circular) */
	double z_limit;			/*!< Spherical clipping Z */

	/* object data storage */
	struct astrodb_slist **objects;		/*!< ptr to object array */
	struct astrodb_tile_status *status;	/*!< ptr to status array */
	int array_size;			/*!< cubic tile array size (in bytes)*/
	int status_size;		/*!< cubic status size (in bytes) */
	int no_tiles;			/*!< number of tiles in cubic array */
	int object_size;		/*!< Object size (bytes) */
	int table_size;      		/*!< Number of objects in set */
	int (*object_insert)(void *table, void* object); /*!< Object insertion method */
	int insert_id;			/*!< Object insertion method ID */
   	
	/* hashed object searching */
	struct hash_map hmaps[HASH_MAPS];
	int num_hmaps;
	
	/* object index description */
	struct astrodb_dlist *byte_description;
	int object_fields;		/*!< number of key and custom fields */
	int alt_fields;                 /*!< number of alt object fields */
	struct astrodb_schema_object idx[TABLE_MAX_IDX];	/*!< key and custom field descriptors */
	struct alt_idx_ alt_idx[TABLE_MAX_AIDX];    /*!< alt src data */
	int record_size;                /*!< length in chars of each record */
	int buffer_size;		/*!< largest import record text size */
};

/*! \typedef astrodb_table_info
 * \brief Dataset binary db index.
 * \ingroup dataset
 *
 * Index data for binary catalog datasets.
 */
struct astrodb_table_index {
	int catalog_magic;		/*!< magic number */
	int endian_magic;		/*!< magic number to determine endianess */
	double ra_min;			/*!< Min catalog RA or AU */
	double ra_max;			/*!< Max catalog RA or AU */
	double dec_min;			/*!< Min catalog DEC */
	double dec_max;			/*!< Max catalog DEC */
	double mag_faint;		/*!< Min (faint) catalog mag */
	double mag_bright;		/*!< Mag (bright) catalog mag */
	int object_size;		/*!< Object size (bytes) */
	int table_size;			/*!< Number of objects in catalog */
	int object_fields;		/*!< number of custom fields */
	int insert_id;			/*!< object importer func */
};

/*! \fn static inline int calc_offset(struct astrodb_table* data, int ra, int dec, int mag)
* \brief Calculate offset into 3d tile array
*/
static inline int table_calc_offset(struct astrodb_table *table, 
						int ra, int dec, int mag)
{
	return ra + (dec * table->ra.stride) + 
		(mag * table->ra.stride * table->dec.stride);
}

/*! \fn static inline int calc_hash(char* data, int len, int mod)
* \brief Calculate hash based on string <data> and in range <mod>
*
* We dont hash ' ' or '-'.
*/
static inline int table_calc_hash_str(char *data, int len, int mod)
{
	int val = 0;

	while (*data != 0) {
		if (*data >= '0' && *data <= 'z') 
			val = ((val << 5) ^ (val >> 27)) ^ *data;	
		data++;
	}

	val = abs(val % mod);
	return val;
}

/*! \fn static inline int calc_hash(char* data, int len, int mod)
* \brief Calculate hash based on string <data> and in range <mod>
*
*/
static inline int table_calc_hash(int val, int mod)
{
	return abs(val % mod);
}

int table_is_field(struct astrodb_table  *table, char *field);
int table_find_field(struct astrodb_table  *table, char *field);
int table_add_alt_field(struct astrodb_table  *table, char *field, int pri_idx);
int table_order_index(struct astrodb_table  *table);
int table_add_custom_struct_field(struct astrodb_table  *table, struct astrodb_schema_object* idx);
void table_set_insert(struct astrodb_table  *table, int id);
astrodb_ctype table_get_ctype(char *type);
int table_get_csize(char *type);
void *table_get_custom_insert(char *type);
void *table_get_key_insert(astrodb_ctype type);
void *table_get_alt_key_insert(astrodb_ctype type);

int table_import(struct astrodb_table  *table, char *file);
int table_cache_tiles(struct astrodb_table  *table);
int table_uncache_tiles(struct astrodb_table  *table);
int table_save_hdr(struct astrodb_table  *table);
int table_load_hdr(struct astrodb_table  *table);
int table_get_remote_dataset(struct astrodb_table  *table);

int table_get_objects_mem(struct astrodb_table  *table, struct astrodb_slist ** result);
int table_get_objects_cache(struct astrodb_table  *table, struct astrodb_slist ** result);
int table_get_objects_raw(struct astrodb_table  *table, struct astrodb_slist ** result);
int table_get_objects_online(struct astrodb_table  *table, struct astrodb_slist ** result);
int table_get_objects_memc(struct astrodb_table  *table, struct astrodb_slist ** result);
int table_get_objects_cachec(struct astrodb_table  *table, struct astrodb_slist ** result);
int table_get_objects_rawc(struct astrodb_table  *table, struct astrodb_slist ** result);
int table_get_objects_onlinec(struct astrodb_table  *table, struct astrodb_slist ** result);
int table_get_objects_cache_near(struct astrodb_table  *table, struct astrodb_slist ** result);
#endif
