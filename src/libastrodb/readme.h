/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2005 Liam Girdwood
 */

#ifndef __LNC_README_H
#define __LNC_README_H

#include <libastrodb/astrodb.h>

struct astrodb_readme_info {
	/* catalog description info from ReadMe */
	char* designation; 		/*!< Catalog designation 1c*/
	char* titles;			/*!< Catalog titles 2c */
	struct astrodb_dlist *keywords;	/*!< Catalog keywords 3c */
	char* description;		/*!< Catalog description 4c */
					/*!< Observed objects 5o */
	struct astrodb_dlist *files;	/*!< Catalog data files 6c */
					/*!< Related catalogs 7o */
					/*!< Nomenclature 8o */
	/* under files */       	/*!< Byte by byte desc 9c */
	struct astrodb_dlist *notes;	/*!< Global notes 10o */
					/*!< History/Acks 11o */
					/*!< References 12o */
					/*!< End (date) */
	struct astrodb_dlist *byte_desc;/*!< byte descriptions */
};

struct astrodb_readme_info *parse_readme(char* file);
void free_readme(struct astrodb_readme_info* info);

#endif
