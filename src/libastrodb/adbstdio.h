/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood  
 */

#ifndef __ADB_ERROR_H
#define __ADB_ERROR_H

#include <stdio.h>
#include <stdarg.h>

extern enum astrodb_msg_level adb_msg_level;

static void adberror_(const char *func, const char *fmt,...)
{
	va_list va;
	va_start(va, fmt);
	fprintf(stderr, "adb-error: %s - ", func);
	vfprintf(stderr, fmt, va);
	va_end(va);
}

#define astrodb_error(format, arg...) \
	adberror_(__func__, format, ## arg)

static void adbout_(const char *level, const char *fmt,...)
{
	va_list va;
	va_start(va, fmt);
	fprintf(stdout, "%s: ", level);
	vfprintf(stdout, fmt, va);
	va_end(va);
}

#define astrodb_info(format, arg...) \
	if (adb_msg_level >= ADB_MSG_INFO) \
		adbout_("adb-info", format, ## arg)

#define astrodb_warn(format, arg...) \
	if (adb_msg_level >= ADB_MSG_WARN) \
		adbout_("adb-warning", format, ## arg)
		
#define astrodb_debug(format, arg...) \
	if (adb_msg_level >= ADB_MSG_DEBUG) \
		adbout_("adb-debug", format, ## arg)

#endif
