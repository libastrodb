/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2005 Liam Girdwood
 */

/*! \mainpage libastrodb
* <A>General Purpose Astronomical Database</A>
* 
* \section intro Introduction
* Libncat is a general purpose astronomical database designed to give very fast
* access to <A href="http://cdsweb.u-strasbg.fr/">CDS</A> catalog data. It is designed to be independent of any underlying 
* catalog data formatting and will import most data records providing the
* catalog ships with a "ReadMe" file that follows the CDS <A href="http://vizier.u-strasbg.fr/doc/catstd.htx">formatting specifications</A>.
* Libncat provides a simple database backend and exposes a C API for catalog access.
*
* The intended audience of libastrodb is C / C++ programmers, astronomers and anyone working with large astronomical catalogs.
*
* Libncat will be the database backend used by the <A href="http://nova.sf.net">Nova</A>
* and <A href="http://astro.corlan.net/gcx/">GCX</A> projects and most importantly, is free software.
*
* \section features Features
* The current release of libastrodb supports:-
*
* - Parsing of important fields in CDS ReadMe files
* - Downloading catalog data from CDS to a local library
* - Importing selected catalog object data and object fields into machine formats.
* - Fast access to catalog data based on :-
* 		- position
*		- position and magnitude
*		- hashed object ID's
*		- distance (near sky catalogs)
* - Progress feedback.
* - Powerfull catalog searching on any combinations of fields.
*
* \section docs Documentation
* API documentation for libastrodb is included in the source. It can also be found in this website and an offline tarball is available <A href="http://libnovacat.sf.net/libastrodbdocs.tar.gz">here</A>.
*
* \section download Download
* The latest release is 0.1 and is available <A href="http://sourceforge.net/project/showfiles.php?group_id=133878">here.</A>
* 
* \section cvs CVS
* The latest CVS version of libastrodb is available via CVS <A href="http://sf.net/cvs/?group_id=133878">here.</A>
*
* \section licence Licence
* libastrodb is released under the <A href="http://www.gnu.org">GNU</A> LGPL.
*
* \section authors Authors
* libastrodb is maintained by <A href="mailto:liam@gnova.org">Liam Girdwood</A>
*
* \section thanks Thanks
* Thanks to Sourceforge for hosting this project. <A href="http://sourceforge.net"> <IMG src="http://sourceforge.net/sflogo.php?group_id=133878&amp;type=5" width="210" height="62" border="0" alt="SourceForge Logo"></A> 
*/

#ifndef __LIBNCAT_H
#define __LIBNCAT_H

#include <libnova/libnova.h>

#ifdef __cplusplus
extern "C" {
#endif
    
/*
 * Dataset initialization flags
 */
#define ADB_MEM		0x1     /*!< Preload the whole dataset into memory */
#define ADB_FILE	0x2     /*!< Store the whole dataset in local disk in binary format */
#define ADB_REFRESH	0x4     /*!< Refresh the dataset with online repo */

/*
 * Error codes
 */
#define ADB_EOK		0		/*!< Status OK, no error */
#define ADB_EIDSET	-1		/*!< Invalid dataset */
#define ADB_EICAT	-2		/*!< Invalid catalog */
#define ADB_EIREADME	-3		/*!< Invalid CDS ReadMe */
#define ADB_ENOFILE	-4		/*!< No such file or directory */
#define ADB_ENOMEM	-5		/*!< No memory */
#define ADB_ENOCATDIR	-6		/*!< Could not create catalog cache dirs */
#define ADB_ENOINS	-7		/*!< No valid object insert found */
#define ADB_ENOFIELD	-8		/*!< Field not valid or found */
#define ADB_ENOFUNC	-9		/*!< Invalid function */
#define ADB_ECMD	-10		/*!< Command failed */
#define ADB_ESRCH	-11		/*!< Invalid search */
#define ADB_ENOOP	-12		/*!< Invalid search operator */
#define ADB_ENOCOMP	-13		/*!< Invalid search comparator */
#define ADB_ENOGET	-14		/*!< Invalid object get */
#define ADB_EINV	-15		/*!< Invalid parameter */
#define ADB_EIMPL	-1000		/*!< Not implemented */	

#define astrodb_offset(x,y) (long)(&((x*)0)->y) 		/* offset in struct */
#define astrodb_sizeof(x,y) sizeof(((x*)0)->y) 		/* size in struct */
#define astrodb_size(x) (sizeof(x)/sizeof(x[0]))		/* array size */

/* convenience index constructors */
#define astrodb_member(lname, lsymbol, lagg, latom, ltype, lunits, lgposn, linsert) \
	{.name = lname, .symbol = lsymbol, .s_offset = astrodb_offset(lagg, latom), \
	 .s_size = astrodb_sizeof(lagg, latom), .units = lunits, .type = ltype, \
	 .units = lunits, .g_posn = lgposn, .insert = linsert,}		
#define astrodb_gmember(lname, lsymbol, lagg, latom, ltype, lunits, lgposn, linsert) \
	{.name = lname, .symbol = lsymbol, .s_offset = astrodb_offset(lagg, latom), \
	 .s_size = astrodb_sizeof(lagg, latom), .units = lunits, .type = ltype, \
	 .units = lunits, .insert = linsert, .g_posn = lgposn, .g_offset = astrodb_offset(lagg, latom),}

struct astrodb_library;
struct astrodb_db;
struct astrodb_table;
struct astrodb_search;
struct astrodb_slist;
struct astrodb_dlist;

/*! \typedef enum astrodb_ctype
 * \ingroup dataset 
 *
 * C type of ASCII dataset field
 */
typedef enum {
	CT_INT,			/*!< int */
	CT_SHORT,		/*!< short */
	CT_DOUBLE, 		/*!< double */
	CT_FLOAT,		/*!< float */ 
	CT_STRING,		/*!< string */
	CT_SIGN,		/*!< sign + or - */
	CT_DOUBLE_HMS_HRS,	/*!< degrees Hours (HMS)*/
	CT_DOUBLE_HMS_MINS,     /*!< degrees Minutes  (HMS)*/ 
	CT_DOUBLE_HMS_SECS,     /*!< degrees Seconds (HMS)*/
	CT_DOUBLE_DMS_DEGS,     /*!< degrees (DMS) */
	CT_DOUBLE_DMS_MINS,     /*!< degrees Minutes (DMS) */
	CT_DOUBLE_DMS_SECS,	/*!< degrees Seconds (DMS) */
	CT_DOUBLE_MPC,		/*!< Minor planet centre date format */
	CT_NULL,		/*!< NULL */
} astrodb_ctype;

/*
 * Object tile status flags
 */
#define ADB_SMEM		0x1	/*!< Memory */
#define ADB_SCACHE		0x2	/*!< Disk cache */
#define ADB_SLOCAL		0x4	/*!< Local (raw) */
#define ADB_SONLINE		0x8	/*!< Online (raw) */
#define ADB_SUSED		0x10	/*!< Used by last get */	


/*! \typedef enum astrodb_operator
 * \ingroup search
 *
 * Search operators
 */
enum astrodb_operator {
	ADB_OP_AND,     /*!< AND */
	ADB_OP_OR       /*!< OR */
};

/*! \typedef enum astrodb_comparator
 * \ingroup search
 *
 * Search field comparators
 */
enum astrodb_comparator {
	ADB_COMP_LT,    /*!< less than */
	ADB_COMP_GT,    /*!< greater than */
	ADB_COMP_EQ,    /*!< equal to */
	ADB_COMP_NE     /*!< not equal to */
};

/*! \typedef enum astrodb_msg_level
 * \ingroup Library
 *
 * AstroDB message level.
 */
enum astrodb_msg_level {
	ADB_MSG_INFO 	= 1,
	ADB_MSG_WARN	= 2,
	ADB_MSG_DEBUG	= 3,
};

/*! \typedef void (*astrodb_func)(void* data, void* user)
 * \brief Function
 */
typedef int (*astrodb_func)(void* data, void* user);

/*! \typedef astrodb_object_status
 * \brief Dataset tile status
 * \ingroup dataset
 */
struct astrodb_tile_status {
	unsigned int flags;	/* source status */
	unsigned int size;	/* number of elements in tile */
};

struct astrodb_slist {
	struct astrodb_slist *tail;
	void* data;
};

struct astrodb_dlist {
	struct astrodb_dlist *head;
	struct astrodb_dlist *tail;
	void* data;
};

#define OBJECT_ID_SIZE		12
#define OBJECT_NAME_SIZE	12


struct astrodb_posn_mag {
	double ra;
	double dec;
	float Vmag;
};

struct astrodb_posn_size {
	double ra;
	double dec;
	float size;
};

struct astrodb_posn_type {
	double ra;
	double dec;
	unsigned int type;
};

struct astrodb_size_type {
	double minor_size;
	double major_size;
	unsigned int type;
};


/*! \struct astrodb_object
 * \brief Astro object.
 *
 * Most objects in are derived from this object. 
 * It contains the following object information:-
 *
 * <ul>
 * <li><b>  Type</b>		<i>Object type (char)</i></li> 
 * <li><b>  Oclass</b>		<i>Object class (char)</i></li>
 * <li><b>  Reserved</b>	<i>Reserved (short)</i></li>
 * <li><b>  ID</b>		<i>ID (12 char)</i></li>
 * <li><b>  Name</b>		<i>Name (12 char)</i></li>
 * </ul>
 */
struct astrodb_object {
	union {
		unsigned long id;
		char designation[OBJECT_NAME_SIZE];
	};
	union {
		struct astrodb_posn_mag posn_mag;
		struct astrodb_posn_size posn_size;
		struct astrodb_posn_type posn_type;
		struct astrodb_size_type size_type;	
	};
};

/*! \struct astrodb_table_info
 * \brief Dataset information.
 *
 * Describes a dataset from a CDS catalog ReadMe file.
 *
 * <ul>
 * <li><b>  Name</b>		<i>Dataset name</i></li> 
 * <li><b>  Records</b>		<i>Number of records in dataset</i></li>
 * <li><b>  Length</b>		<i>Length of dataset records</i></li>
 * <li><b>  Title</b>		<i>Dataset title</i></li>
 * <li><b>  Byte Desc</b>	<i>Dataset byte description (in dlist)</i></li>
 * </ul>
 */
struct astrodb_table_info {
	char* name;		  	/*!< filename dos 8.3 */
	int records;			/*!< number of records */
	int length;			/*!< maximum line length */
	char* title;			/*!< short title (80 chars max) */
	struct astrodb_dlist* byte_description;	/*!< Byte by byte desc 9c */
};


/*! \struct astrodb_table_column_info
 * \brief Dataset byte description.
 *
 * Describes a field from a CDS dataset.
 *
 * <ul>
 * <li><b>  Start</b>		<i>Field start pos</i></li> 
 * <li><b>  End</b>		<i>Field end pos</i></li>
 * <li><b>  Type</b>		<i>Field type</i></li>
 * <li><b>  Units</b>		<i>Units</i></li>
 * <li><b>  Label</b>		<i>CDS label</i></li>
 * <li><b>  Explanation</b>	<i>Explanation</i></li>
 * </ul>
 */
struct astrodb_table_column_info { 
	short start;        /*!< start byte */
	short end;          /*!< end byte */
	char *type;         /*!< field type */
	char *units;        /*!< field units */
	char *label;        /*!< field label */
	char *explanation;  /*!< field explanation */
};

#define ADB_SCHEMA_NAME_SIZE		32
#define ADB_SCHEMA_SYMBOL_SIZE		8
#define ADB_SCHEMA_UNITS_SIZE		8

/*! \struct astrodb_schema_object
 * \ingroup dataset
 *
 * Represents a field within a dfataset structure.
 */
struct astrodb_schema_object {
	char name[ADB_SCHEMA_NAME_SIZE];		/*!< field name */
	char symbol[ADB_SCHEMA_SYMBOL_SIZE];		/*!< field symbol */
	int s_offset;		/*!< struct offset */
	int s_size;		/*!< struct size */
	int g_offset;		/*!< group offset, -1 if atomic */
	int g_posn;		/*!< group posn, lowest is first */
	int l_size;		/*!< line size */
	int l_offset;		/*! line offset */
	astrodb_ctype type;		/*!< field type */
	char units[ADB_SCHEMA_UNITS_SIZE];		/*!< field units */
	astrodb_func insert;	/* custom insert method */
};

/*! \fn astrodb_library* astrodb_open_library(char *remote, char* local);
 * \brief Create a library
 * \ingroup library
 */
void astrodb_set_msg_level(enum astrodb_msg_level level);

/*! \fn astrodb_library* astrodb_open_library(char *remote, char* local);
 * \brief Create a library
 * \ingroup library
 */
struct astrodb_library* astrodb_open_library(char *remote, char* local);

/*! \fn void astrodb_close_library(astrodb_library* lib);
 * \brief Free the library resources
 * \ingroup library
 */
void astrodb_close_library(struct astrodb_library *lib);

/*! \fn astrodb_db* astrodb_create_db(astrodb_library* lib, char* cclass, char* cnum, 
	double ra_min, double ra_max,double dec_min, double dec_max, 
	double mag_faint, double mag_bright, int flags);
 * \brief Create a new catalog 
 * \ingroup catalog
 */
struct astrodb_db* astrodb_create_db(struct astrodb_library *lib, 
				char* cat_class, char* cat_num, 
				double ra_min, double ra_max, double dec_min, 
				double dec_max, double mag_faint, 
				double mag_bright, int flags);

/*! \fn astrodb_dlist* astrodb_db_get_tables(astrodb_db* cat);
 * \ingroup catalog
 * \brief Query the datasets in the catalog
 */
struct astrodb_dlist* astrodb_db_get_tables(struct astrodb_db *db);

/*! \fn void astrodb_db_free (astrodb_db* cat);
 * \brief Destroys catalog and frees resources
 * \ingroup catalog
 */
void astrodb_db_free(struct astrodb_db *db);

/*! \fn const char* astrodb_get_version(void);
 * \brief Get the libastrodb version number.
 * \ingroup misc
 */
const char *astrodb_get_version(void);

/*! \fn const char* astrodb_get_last_err_msg(void);
 * \brief Get the text of the last error.
 * \ingroup misc
 */
const char* astrodb_get_err_str(int err);

/*! \fn int astrodb_table_register_schema(astrodb_table *table, astrodb_schema_object* idx, 
					int idx_size);
 * \brief Register a new astro object type
 * \ingroup catalog 
 */
int astrodb_table_register_schema(struct astrodb_table *table,	
					struct astrodb_schema_object *schema, 
					int schema_size, int object_size);
	
/*! \fn astrodb_table* astrodb_table_create(astrodb_db* cat, char *table_name,
					unsigned int flags);
 * \brief Create a new dataset
 * \ingroup dataset
 */
struct astrodb_table* astrodb_table_create(struct astrodb_db *db, 
					char* table_name, 
					unsigned int create_flags);

/*! \fn astrodb_table_open(astrodb_table *table, astrodb_progress progress, int ra, 
			int dec, int mag)
 * \brief Initialise new dataset.
 * \ingroup dataset
 */
int astrodb_table_open(struct astrodb_table *table,
			int ra, int dec, int mag);

/*! \fn void astrodb_table_close(astrodb_table *table);
 * \brief Free's dataset and it's resources
 * \ingroup dataset
 */
void astrodb_table_close(struct astrodb_table *table);

#if 0
/*! \fn int astrodb_dset_add_custom_field(astrodb_table *table, char* field);
* \brief Add a custom field to the dataset for importing
* \ingroup dataset
*/
int astrodb_dset_add_custom_field(astrodb_table *table, char* field);
#endif

/*! \fn int astrodb_table_hash_key(astrodb_table *table, char* field);
* \brief Add a custom field to the dataset for importing
* \ingroup dataset
*/
int astrodb_table_hash_key(struct astrodb_table *table, char *key);

/*! \fn int astrodb_table_alt_column(astrodb_table *table, char* field, char* alt,
					int flags);
* \brief Set an alternative field if another field is blank
* \ingroup dataset
*/
int astrodb_table_alt_column(struct astrodb_table *table, 
				char *field, char *alt, int flags);

/*! \fn void astrodb_table_clip_on_position (astrodb_table *table, double min_ra, 
					double min_dec, double max_ra,
					double max_dec, double faint_mag,
					double bright_mag);
 * \brief Set dataset clipping area based on position
 * \ingroup dataset
 */
void astrodb_table_clip_on_position(struct astrodb_table *table, 
				double min_ra, double min_dec, 
				double max_ra, double max_dec,
				double faint_mag, double bright_mag);
                     
/*! \fn void astrodb_table_clip_on_distance (astrodb_table *table, double min_AU,
				double max_AU);
 * \brief Set dataset clipping area based on distance
 * \ingroup dataset
 */
void astrodb_table_clip_on_distance(struct astrodb_table *table, 
				double min_AU, double max_AU);

/*! \fn void astrodb_table_clip_on_fov (astrodb_table *table, double ra, double dec, 
					double fov, double faint_mag,
					double bright_mag);
 * \brief Set dataset clipping area based on field of view
 * \ingroup dataset
 */
void astrodb_table_clip_on_fov(struct astrodb_table *table, 
				double ra, double dec, 
				double fov, double faint_mag,
				double bright_mag);

/*! \fn void astrodb_table_unclip (astrodb_table *table);
 * \brief Unclip dataset clipping area to full boundaries
 * \ingroup dataset
 */
void astrodb_table_unclip(struct astrodb_table *table);

/*! \fn int astrodb_table_get_objects (astrodb_table *table, astrodb_progress progress, 
					astrodb_slist **results, unsigned int src); 
 * \brief Get objects from catalog based on clipping area
 * \return number of objects, or negative for failed
 * \ingroup dataset
 */
int astrodb_table_get_objects(struct astrodb_table *table, 
				struct astrodb_slist **results, 
				unsigned int src); 

/*! \fn void astrodb_table_put_objects (astrodb_slist *results);
 * \brief Release slist memory from astrodb_table_get_objects()
 * \ingroup dataset
 */
void astrodb_table_put_objects(struct astrodb_slist *results);

/*! \fn void* astrodb_table_get_object (astrodb_table *table, char *id, char *field);
 * \brief Get object from catalog based on ID
 * \return pointer to object or NULL
 * \ingroup dataset
 */
void* astrodb_table_get_object(struct astrodb_table *table, 
				char *id, char *field);

/* some catalog management functionality to manage memory and access */

/*! \fn int astrodb_table_prune(astrodb_table *table);
 * \brief free all old sectors.
 * \return bytes free'd
 * \ingroup dataset
 */
int astrodb_table_prune(struct astrodb_table *table);

/*! \fn int astrodb_table_get_size(astrodb_table *table);
 * \brief Get dataset cache size.
 * \return size in bytes
 * \ingroup dataset
 */
int astrodb_table_get_size(struct astrodb_table *table);

/*! \fn int astrodb_table_get_row_size(astrodb_table *table);
 * \brief Get dataset object size.
 * \return size in bytes
 * \ingroup dataset
 */
int astrodb_table_get_row_size(struct astrodb_table *table);

#if 0
/*! \fn void astrodb_dset_set_status_posn(astrodb_table *table, double min_ra, 
		double min_dec, double max_ra, double max_dec,
		double faint_mag,double bright_mag, unsigned int status);
 * \brief set sector status based upon area
 * \ingroup dataset
 */
void astrodb_dset_set_status_posn(astrodb_table *table, double min_ra, double min_dec, 
				double max_ra, double max_dec, double faint_mag,
				 double bright_mag, unsigned int status);

/*! \fn void astrodb_dset_set_status_dist(astrodb_table *table, double min_AU,
					double max_AU, unsigned int status);
 * \brief set sector status based upon area
 * \ingroup dataset
 */
void astrodb_dset_set_status_dist(astrodb_table *table, double min_AU, double max_AU, 
				unsigned int status);

/*! \fn astrodb_tile_status astrodb_dset_get_status_posn(astrodb_table *table,
					double ra, double dec, double mag);
 * \brief get sector status
 * \ingroup dataset
 */
astrodb_tile_status astrodb_dset_get_status_posn(astrodb_table *table, double ra,
						double dec, double mag);

/*! \fn astrodb_tile_status astrodb_dset_get_status_dist (astrodb_table *table, double AU);
 * \brief get sector status
 * \ingroup dataset
 */
astrodb_tile_status astrodb_dset_get_status_dist(astrodb_table *table, double AU);
#endif
/*! \fn astrodb_ctype astrodb_table_get_column_type(astrodb_table *table, char* field);
 * \brief Get field type in struct
 * \ingroup dataset
 */
astrodb_ctype astrodb_table_get_column_type(struct astrodb_table *table, 
					char* field);

/*! \fn int astrodb_table_get_column_offset(astrodb_table *table, char* field);
 * \brief Get field offset in struct
 * \ingroup dataset
 */
int astrodb_table_get_column_offset(struct astrodb_table *table, char* field);

/*! \fn int astrodb_table_for_search_results_do(astrodb_slist* res, 
		void(*func)(void* object, void* data), void* data)
 * \brief Do func for every object in result
 * \ingroup dataset
 */
int astrodb_table_for_search_results_do(struct astrodb_slist* res, 
				void(*func)(void* object, void* data),
				void* data);

/*! \typedef int (*astrodb_custom_comparator)(void* object);
 * 
 * A customised object search comarator.
 */
typedef int (*astrodb_custom_comparator)(void* object);

 /*! \fn astrodb_search* astrodb_search_create(astrodb_table *table);
 * \brief Creates an new search object
 * \ingroup search
 */
struct astrodb_search* astrodb_search_create(struct astrodb_table *table);

/*! \fn void astrodb_search_free(astrodb_search* search);
 * \brief Free's a search and it resources
 * \ingroup search
 */
void astrodb_search_free(struct astrodb_search* search);

/*! \fn int astrodb_search_add_operator(astrodb_search* search, astrodb_operator op);
 * \brief Add an operation in RPN to the search
 * \ingroup search
 */
int astrodb_search_add_operator(struct astrodb_search *search, 
				enum astrodb_operator op);

/*! \fn int astrodb_search_add_comparator(astrodb_search* search, char* field,
					astrodb_comparator comp, char* value);
 * \brief Add a comparator in RPN to the search
 * \ingroup search 
 */
int astrodb_search_add_comparator(struct astrodb_search* search, char* field,
				enum astrodb_comparator comp, char* value);
					   
/*! \fn int astrodb_search_add_custom_comparator(astrodb_search* search,
						astrodb_custom_comparator comp);
 * \brief Add a comparator in RPN to the search
 * \ingroup search
 */
int astrodb_search_add_custom_comparator(struct astrodb_search* search,
					astrodb_custom_comparator comp);

/*! \fn int astrodb_search_get_results(astrodb_search* search, astrodb_progress progress,
					astrodb_slist **result, unsigned int src); 
 * \brief Execute a search
 * \ingroup search
 */
int astrodb_search_get_results(struct astrodb_search* search,
				struct astrodb_slist **result, unsigned int src); 

/*! \fn void astrodb_search_put_results (astrodb_slist *results);
 * \brief Release the search results
 * \ingroup search
 */
void astrodb_search_put_results(struct astrodb_slist *results);

/*! \fn int astrodb_search_get_hits(astrodb_search* search);
 * \brief Get the number of search hits.
 * \ingroup search
 */
int astrodb_search_get_hits(struct astrodb_search* search);

/*! \fn int astrodb_search_get_tests(astrodb_search* search);
 * \brief Get the number of search tests
 * \ingroup search
 */
int astrodb_search_get_tests(struct astrodb_search* search);

/*! \fn astrodb_slist* astrodb_slist_prepend(astrodb_slist* s, void* object);
 * \brief Prepend an object to a slist
 * \ingroup list
 */
struct astrodb_slist* astrodb_slist_prepend(struct astrodb_slist* s, void* object);

/*! \fn astrodb_slist* astrodb_slist_append(astrodb_slist* s, void* object);
 * \brief Append an object to a slist
 * \ingroup list
 */
struct astrodb_slist* astrodb_slist_append(struct astrodb_slist* s, void* object);

/*! \def astrodb_slist_next(s);
 * \ingroup list
 * 
 * Get the next node in the slist \a s
 */
#define astrodb_slist_next(s) s->tail

/*! \def astrodb_dlist_next(d);
 * \ingroup list
 * 
 * Get the next node in the dlist \a d
 */
#define astrodb_dlist_next(d) d->tail

/*! \def astrodb_dlist_prev(d);
 * \ingroup list
 * 
 * Get the prevoius node in the dlist \a d
 */
#define astrodb_dlist_prev(d) d->head

/*! \fn void astrodb_slist_foreach(astrodb_slist* sl , astrodb_func func, void* user);
 * \brief Call a function for every item in slist
 * \ingroup list
 */
void astrodb_slist_foreach(struct astrodb_slist* sl , astrodb_func func, void* user);

/*! \fn void astrodb_slist_foreach_free(astrodb_slist* sl , astrodb_func func, void* user);
 * \brief Call a function for every item in slist and then free item
 * \ingroup list
 */
void astrodb_slist_foreach_free(struct astrodb_slist* sl , astrodb_func func, void* user);

/*! \fn astrodb_dlist* astrodb_dlist_prepend(astrodb_dlist* d, void* object);
 * \brief Prepend an object to a dlist
 * \ingroup list
 */
struct astrodb_dlist* astrodb_dlist_prepend(struct astrodb_dlist* d, void* object);

/*! \fn astrodb_dlist* astrodb_dlist_append(astrodb_dlist* d, void* object);
 * \brief Append an object to a dlist
 * \ingroup list
 */
struct astrodb_dlist* astrodb_dlist_append(struct astrodb_dlist* d, void* object);

/*! \fn astrodb_dlist* astrodb_dlist_head(astrodb_dlist* d);
 * \brief Get the head of a dlist
 * \ingroup list
 */
struct astrodb_dlist* astrodb_dlist_head(struct astrodb_dlist* d);

/*! \fn astrodb_dlist* astrodb_dlist_free_object(astrodb_dlist* d, void* object);
 * \brief Free an object from a dlist
 * \ingroup list
 */
struct astrodb_dlist* astrodb_dlist_free_object(struct astrodb_dlist* d, void* object);

/*! \fn void astrodb_dlist_foreach(astrodb_dlist* dl , astrodb_func func, void* user);
 * \brief Call a function for every item in dlist
 * \ingroup list
 */
void astrodb_dlist_foreach(struct astrodb_dlist* dl , astrodb_func func, void* user);

/*! \fn void astrodb_dlist_foreach_free(astrodb_dlist* dl , astrodb_func func, void* user);
 * \brief Call a function for every item in dlist and the free item
 * \ingroup list
 */
void astrodb_dlist_foreach_free(struct astrodb_dlist* dl , astrodb_func func, void* user);


#ifdef __cplusplus
};
#endif

#endif
