/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood
 */

#define _GNU_SOURCE		/* for strtof and NAN */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <ctype.h>

#include <libastrodb/db.h>
#include <libastrodb/table.h>
#include <libastrodb/adbstdio.h>

#define FILE_NAME_SIZE 	256
#define FLOAT_SIZE 	'6'  /* CDS F size and below for a float else double */
#define IMPORT_LINE_SIZE	1024

/*
 * libastrodb interface version
 */
#define ADB_IDX_VERSION 2

/*! \fn static int insert_posn_mag (void* d, void* object)
 * \brief Insert object into dataset based upon position and mag
 */
int insert_posn_mag(void *table_, void *object)
{
	int ra, dec, mag, offset;
	struct astrodb_table *table = (struct astrodb_table *) table_;
	struct astrodb_object *dobject = object;

	if (dobject->posn_mag.ra < table->db->ra_min || 
		dobject->posn_mag.ra > table->db->ra_max)
		return 0;
	if (dobject->posn_mag.dec < table->db->dec_min || 
		dobject->posn_mag.dec > table->db->dec_max)
		return 0;
	if (dobject->posn_mag.Vmag > table->db->mag_faint || 
		dobject->posn_mag.Vmag < table->db->mag_bright)
		return 0;

	ra = floor((dobject->posn_mag.ra - table->db->ra_min) / 
		table->ra.stride_size);
	dec = floor((dobject->posn_mag.dec - table->db->dec_min) / 
		table->dec.stride_size);
	mag = floor((dobject->posn_mag.Vmag - table->db->mag_bright) / 
		table->mag.stride_size);

	offset = table_calc_offset(table, ra, dec, mag);

	if (offset < 0) {
		astrodb_error("failed to calculate offset for RA %3.2f"
			" DEC %3.2f mag %3.2f\n", ra, dec, mag);
		return 0;
	}
	
	*(table->objects + offset) = 
		astrodb_slist_prepend(*(table->objects + offset), object);
	(table->status + offset)->flags = ADB_SMEM;
	(table->status + offset)->size++;
	return 1;
}

/*! \fn static int insert_posn_type (void* d, void* object)
 * \brief Insert object into dataset based upon position and type
 */
static int insert_posn_type(void *table_, void *object)
{
	int ra, dec, type, offset;
	struct astrodb_table *table = (struct astrodb_table *) table_;
	struct astrodb_object *dobject = object;
	
	if (dobject->posn_type.ra < table->db->ra_min || 
		dobject->posn_type.ra > table->db->ra_max)
		return 0;
	if (dobject->posn_type.dec < table->db->dec_min || 
		dobject->posn_type.dec > table->db->dec_max)
		return 0;
	
	ra = floor((dobject->posn_type.ra - table->db->ra_min) / 
		table->ra.stride_size);
	dec = floor((dobject->posn_type.dec - table->db->dec_min) / 
		table->dec.stride_size);
	type = dobject->posn_type.type;
	
	offset = table_calc_offset(table, ra, dec, type);
	if (offset < 0) {
		astrodb_error("failed to calculate offset for RA %3.2f"
			" DEC %3.2f type %3.2f\n", ra, dec, type);
		return 0;
	}
	
	*(table->objects + offset) = 
		astrodb_slist_prepend(*(table->objects + offset), object);
	(table->status + offset)->flags = ADB_SMEM;
	(table->status + offset)->size++;
	return 1;
}

/*! \fn static int insert_hash_id (void* d, void* object)
 * \brief Insert object into dataset based upon position and mag
 */
static int insert_hash_id(void *table_, void *object)
{
	struct astrodb_table *table = (struct astrodb_table *) table_;
	int offset = 
		table_calc_hash(((struct astrodb_object *) object)->id, 
					table->no_tiles);
	
	*(table->objects + offset) = 
			astrodb_slist_prepend(*(table->objects + offset), object);
	(table->status + offset)->flags = ADB_SMEM;
	(table->status + offset)->size++;
	return 1;
}

/*! \fn static inline int insert_hash_id_hobject(astrodb_table *table, void *object, int hid)
 * \brief Insert object into dataset based upon position and mag
 */
static inline int insert_hash_id_hobject(struct astrodb_table *table, 
						void *object, int hid)
{
	int offset;
	
	offset = table_calc_hash_str((char *) object + table->hmaps[hid].offset,
				    OBJECT_ID_SIZE, table->no_tiles);
	
	*(table->hmaps[hid].hobjects + offset) = 
		astrodb_slist_prepend(*(table->hmaps[hid].hobjects + offset), object);
	return 1;
}

/*! \fn static int insert_posn_mag_hash (void* d, void* object)
 * \brief Insert object into dataset based upon position and mag
 */
static int insert_posn_mag_hash(void *d, void *object)
{
	int ra, dec, mag, offset;
	struct astrodb_table *table = (struct astrodb_table *) d;
	struct astrodb_object *dobject = object;
	int i;
	
	if (dobject->posn_mag.ra < table->db->ra_min || 
		dobject->posn_mag.ra > table->db->ra_max)
		return 0;
	if (dobject->posn_mag.dec < table->db->dec_min || 
		dobject->posn_mag.dec > table->db->dec_max)
		return 0;
	if (dobject->posn_mag.Vmag > table->db->mag_faint || 
		dobject->posn_mag.Vmag < table->db->mag_bright)
		return 0;
	
	ra = floor((dobject->posn_mag.ra - table->db->ra_min) / 
		table->ra.stride_size);
	dec = floor((dobject->posn_mag.dec - table->db->dec_min) / 
		table->dec.stride_size);
	mag = floor((dobject->posn_mag.Vmag - table->db->mag_bright) / 
		table->mag.stride_size);
	
	offset = table_calc_offset(table, ra, dec, mag);
	if (offset < 0) {
		astrodb_error("failed to calculate deep offset for RA %3.2f"
			" DEC %3.2f mag %3.2f\n", ra, dec, mag);
		return 0;
	}
	
	*(table->objects + offset) = 
		astrodb_slist_prepend(*(table->objects + offset), object);
	(table->status + offset)->flags = ADB_SMEM;
	(table->status + offset)->size++;
	
	for (i = 0; i < table->num_hmaps; i++)
		insert_hash_id_hobject(table, object, i);
	
	return 1;
}

void table_get_object_insert(struct astrodb_table *table)
{
	// TODO use standard IDs
	if (table->num_hmaps > 0)
		table->object_insert = &insert_posn_mag_hash;
	else
		table->object_insert = &insert_posn_mag;
}

/* table type insert's */
static int int_insert(void *dest, void *src)
{
	char *ptr;
	
	*(int *) dest = strtol(src, &ptr, 10);
	if (src == ptr)
		return -1;
	return 0;
}
static int short_insert(void *dest, void *src)
{
	char *ptr;
	
	*(short *) dest = strtol(src, &ptr, 10);
	if (src == ptr)
		return -1;
	return 0;
}
static int float_insert(void *dest, void *src)
{
	char *ptr;
	
	*(float *) dest = strtof(src, &ptr);
	if (src == ptr) {
		*(float*) dest = FP_NAN;
		return -1;
	}
	return 0;
}
static int double_insert(void *dest, void *src)
{
	char *ptr;
	
	*(double *) dest = strtod(src, &ptr);
	if (src == ptr) {
		*(double*) dest = FP_NAN;
		return -1;
	}
	return 0;
}
static int str_insert(void *dest, void *src)
{
	/* do not overrun our string */
	strncpy(dest, src, strlen(src));
	return 0;
}

static int double_dms_mins(void *dest, void *src)
{
	char *ptr;
	
	*(double *) dest += strtod(src, &ptr) / 60.0;
	if (src == ptr)
		return -1;
	return 0;
}
static int double_dms_secs(void *dest, void *src)
{
	char *ptr;
	
	*(double *) dest += strtod(src, &ptr) / 3600.0;

	if (src == ptr)
		return -1;
	return 0;
}
static int sign_insert(void *dest, void *src)
{
	if (*(char*)src == '-')
		*(double *) dest *= -1.0;
	return 0;
}
static int double_hms_hrs(void *dest, void *src)
{
	char *ptr;
	
	*(double *) dest = strtod(src, &ptr) * 15.0;

	if (src == ptr)
		return -1;
	return 0;
}
static int double_hms_mins(void *dest, void *src)
{
	char *ptr;
	
	*(double *) dest += strtod(src, &ptr) * 15.0 / 60.0;
	if (src == ptr)
		return -1;
	return 0;
}
static int double_hms_secs(void *dest, void *src)
{
	char *ptr;
	
	*(double *) dest += strtod(src, &ptr) * 15.0 / 3600.0;
	if (src == ptr)
		return -1;
	return 0;
}

static int float_alt_insert(void *dest, void *src, void *src2)
{
	char *ptr;
	
	*(float *) dest = strtof(src, &ptr);
	
	/* is primary source invalid then try alternate source */
	if (src == ptr) {
		*(float *) dest = strtof(src2, &ptr);
		if (src2 == ptr) {
			*(float*) dest = FP_NAN;
			return -1;
		}
	}
	return 0;
}

static int double_alt_insert(void *dest, void *src, void *src2)
{
	char *ptr;
	
	*(double *) dest = strtod(src, &ptr);
	
	/* is primary source invalid then try alternate source */
	if (src == ptr) {
		*(double *) dest = strtod(src2, &ptr);
		if (src2 == ptr) {
			*(double *) dest = FP_NAN;
			return -1;
		}
	}
	return 0;
}

/*! \fn astrodb_ctype table_get_ctype(char *type)
 * \brief Get C type from ASCII type
 */
astrodb_ctype table_get_ctype(char *type)
{
	if (*type == 'I')
		return CT_INT;
	if (*type == 'A')
		return CT_STRING;
	if (*type == 'F') {
		if (*(type + 1) > FLOAT_SIZE)
			return CT_DOUBLE;
		else
			return CT_FLOAT;
	}
	return CT_NULL;
}

/*! \fn int table_get_csize(char *type)
 * \brief Get C size from ASCII size
 */
int table_get_csize(char *type)
{
	if (*type == 'I')
		return sizeof(int);
	if (*type == 'A')
		return strtol(type + 1, NULL, 10);
	if (*type == 'F') {
		if (*(type + 1) > FLOAT_SIZE)
			return sizeof(double);
		else
			return sizeof(float);
	}
	return 0;
}

/*! \fn void *table_get_key_insert(astrodb_ctype type)
 * Get dataset type insert
 */
void *table_get_key_insert(astrodb_ctype type)
{
	switch (type) {
	case CT_DOUBLE_DMS_DEGS:
	case CT_DOUBLE:
		return double_insert;
	case CT_INT:
		return int_insert;
	case CT_SHORT:
		return short_insert;
	case CT_STRING:
		return str_insert;
	case CT_FLOAT:
		return float_insert;
	case CT_DOUBLE_DMS_MINS:
		return double_dms_mins;
	case CT_DOUBLE_DMS_SECS:
		return double_dms_secs;
	case CT_SIGN:
		return sign_insert;
	case CT_DOUBLE_MPC:
	case CT_NULL:
		return NULL;
	case CT_DOUBLE_HMS_HRS:
		return double_hms_hrs;
	case CT_DOUBLE_HMS_MINS:
		return double_hms_mins;
	case CT_DOUBLE_HMS_SECS:
		return double_hms_secs;
	}
	astrodb_error("Invalid column insert %d\n", type);
	return NULL;
}

/*! \fn void *table_get_alt_key_insert(astrodb_ctype type)
 * Get dataset type insert
 */
void *table_get_alt_key_insert(astrodb_ctype type)
{
	switch (type) {
	case CT_DOUBLE:
		return double_alt_insert;
	case CT_FLOAT:
		return float_alt_insert;
	case CT_INT:
	case CT_STRING:
	case CT_DOUBLE_DMS_DEGS:
	case CT_DOUBLE_DMS_MINS:
	case CT_DOUBLE_DMS_SECS:
	case CT_SIGN:
	case CT_DOUBLE_MPC:
	case CT_NULL:
	case CT_DOUBLE_HMS_HRS:
	case CT_DOUBLE_HMS_MINS:
	case CT_DOUBLE_HMS_SECS:
		astrodb_error("Invalid alt column insert %d\n", type);
		return NULL;
	}
	return NULL;
}

static void get_import_buffer_size(struct astrodb_table *table)
{
	int i;
	
	table->buffer_size = 0;
	
	for (i = 0; i < table->object_fields; i++) {	
		if (table->idx[i].l_size > table->buffer_size)
			table->buffer_size = table->idx[i].l_size;
	}
}

static inline void dump(struct astrodb_object *object)
{
	astrodb_debug("id %8d %8.8f %8.8f %8.8f\n", 
		object->id, object->posn_mag.ra, 
		object->posn_mag.dec, object->posn_mag.Vmag);
}

static int import_data(struct astrodb_table *table, FILE *f)
{
	int i, j, count = 0, short_records = 0, insert, warn;
	char *line;
	char buf[IMPORT_LINE_SIZE];
	void *object;
	size_t size;
	ssize_t rsize;
	
	line = malloc(IMPORT_LINE_SIZE);
	if (line == NULL)
		return -ENOMEM;
	
	astrodb_info("Starting import with object size %d bytes\n",
	       table->object_size);
	
	size = table->record_size + 10;
	for (j = 0; j < table->table_size; j++) {
		warn = 0;
		object = malloc(table->object_size);
		if (object == NULL) {
			free(line);
			return -ENOMEM;
		}
		bzero(object, table->object_size);
		bzero(line, IMPORT_LINE_SIZE);
		
		/* try and read a little extra padding */
		rsize = getline(&line, &size, f);
			
		if (rsize < table->record_size)
			short_records++;
		
		/* create row by inserting column (field) data */
		for (i = 0; i < table->object_fields; i++) {
			bzero(buf, table->buffer_size);
			strncpy(buf, line + table->idx[i].l_offset, 
				table->idx[i].l_size);

			insert = table->idx[i].
				insert(object + table->idx[i].s_offset, buf);
			if (insert < 0) {
				astrodb_warn(" blank field %s\n", 
					table->idx[i].symbol);
				warn = 1;
			}
		}
		
		if (warn) {
			astrodb_warn("At line %d :-\n", count);
			astrodb_warn("line %s\n\n", line);
		}
		
		/* insert row into table */
		count += table->object_insert(table, object);		
		dump(object);
	}
	astrodb_info("Got %d short records\n", short_records);
	astrodb_info("Imported %d records\n", count);
	free(line);
	return count;
}

static int import_data_alt(struct astrodb_table *table, FILE *f)
{
	int i, j, k, count = 0, short_records = 0, warn, insert;
	char *line;
	char buf[IMPORT_LINE_SIZE], buf2[IMPORT_LINE_SIZE];
	void *object;
	size_t size;
	ssize_t rsize;
	
	line = malloc(IMPORT_LINE_SIZE);
	if (line == NULL)
		return -ENOMEM;
	
	astrodb_info("Starting import with object size %d bytes\n", 
	       table->object_size);
	astrodb_info("Importing %d alt fields\n", table->alt_fields);
	
	size = table->record_size + 10;
	for (j = 0; j < table->table_size; j++) {
		object = calloc(1, table->object_size);
		if (object == NULL) {
			free(line);
			return -ENOMEM;
		}
		warn = 0;
		bzero(object, table->object_size);
		bzero(line, IMPORT_LINE_SIZE);
		
		/* try and read a little extra padding */
		rsize = getline(&line, &size, f);
		
		if (rsize < table->record_size)
			short_records++;
		
		/* create row by inserting colunm (field) data */
		for (i = 0; i < table->object_fields; i++) {
			bzero(buf, table->buffer_size);
			strncpy(buf, line + table->idx[i].l_offset, 
				table->idx[i].l_size);
			insert = table->idx[i].insert(
				object + table->idx[i].s_offset, buf);
			if (insert < 0) {
				astrodb_warn(" blank field %s\n", 
					table->idx[i].symbol);
				warn = 1;
			}	
		}
		
		/* complete row by inserting alt column (field) data */
		for (k = 0; k < table->alt_fields; k++) {
			bzero(buf, table->buffer_size);
			bzero(buf2, table->buffer_size);
			strncpy(buf, line + table->alt_idx[k].pri.l_offset,
				table->alt_idx[k].pri.l_size);
			strncpy(buf2, line + table->alt_idx[k].sec.l_offset,
				table->alt_idx[k].sec.l_size);
			insert = table->alt_idx[k].insert(object + 
						table->alt_idx[k].pri.s_offset, 
						buf, buf2);
			if (insert < 0) {
				astrodb_warn(" blank fields %s %s\n", 
					table->alt_idx[k].pri.symbol,
					table->alt_idx[k].sec.symbol);
				warn = 1;
			}
		}
		
		if (warn) {
			astrodb_warn("At line %d :-\n", count);
			astrodb_warn("line %s\n\n", line);
		}
				
		/* insert row into table */
		count += table->object_insert(table, object);
	}
	astrodb_info("Got %d short records\n", short_records);
	astrodb_info("Imported %d records\n", count);
	free(line);
	return count;
}

/*! \fn int table_import(astrodb_table * table, char *file, astrodb_progress progress)
 * \brief Import an ASCII dataset into table tile array
 */
int table_import(struct astrodb_table *table, char *file)
{
	FILE *f = NULL;
	
	if (!table->object_insert) {
		astrodb_error("Invalid object insert\n");
		return -EINVAL;
	}
	
	f = fopen(file, "r");
	if (f == NULL) {
		astrodb_error("failed to open file %s\n", file);
		return -EIO;
	}
	
	table_order_index(table);
	
	get_import_buffer_size(table);
	
	if (table->alt_fields)
		import_data_alt(table, f);
	else
		import_data(table, f);
	
	fclose(f);
	return 0;
}
