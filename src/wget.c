/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood 
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>

#include <libastrodb/db.h>
#include <libastrodb/library.h>
#include <libastrodb/adbstdio.h>

#define FILE_LENGTH	1024

int wget_file(struct astrodb_db *db, char *file)
{
	int pid = 0;
	int ret = 0;
	int fd[2];
	char dest[FILE_LENGTH];
	char src[FILE_LENGTH];
	int status;
	
	ret = pipe(fd);
	if (ret < 0) {
		astrodb_error("failed to open pipe %d\n", ret);
		return ret;
	}
	
	strcpy(dest, "--directory-prefix=");
	strncat(dest, db->local_path, FILE_LENGTH - strlen(dest));
	
	strncpy(src, db->remote_path, FILE_LENGTH);
	strncat(src, file, FILE_LENGTH - strlen(src));
	
	pid = vfork();
	if (pid == 0) {
		/* child */
		close(fd[0]);
		if (fd[1] != STDERR_FILENO)
			dup2(fd[1], STDERR_FILENO);
		astrodb_info("wget %s to %s\n", src, dest);
		ret = execlp("wget", "-nc", "--progress=dot:force", 
				&dest, &src, NULL);
		if (ret < 0) {
			astrodb_error("failed to wget %s %d\n", src, ret);
			exit(ret);
		}
	} else {
		/* parent */
		close(fd[1]);
		if (fd[0] != STDIN_FILENO)
			dup2(fd[0], STDIN_FILENO);
		
		wait(&status);
	}
	if (WIFEXITED(status)) {
		if (WEXITSTATUS(status))
			return -WEXITSTATUS(status);
		else
			return 0;
	}
	
	/* wget failed */
	astrodb_error("wget fork failed\n");
	return -EIO;
}
