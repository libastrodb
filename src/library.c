/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood
 */

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include <libastrodb/library.h>
#include <libastrodb/adbstdio.h>

#define PATH_SIZE 1024

enum astrodb_msg_level adb_msg_level = ADB_MSG_INFO;

static char *dirs[] = {
	"/I",
	"/II",
	"/III",
	"/IV",
	"/V",
	"/VI",
	"/VII",
	"/VIII",
	"/IX",
};

static int create_lib_local_dirs(char *location)
{
	char dir[PATH_SIZE];
	struct stat stat_info;
	int i = 0, ret = 0;
	
	/* create <path>/lnc/ */
	bzero(dir, PATH_SIZE);
	strncat(dir, location, PATH_SIZE);
	if (stat(location, &stat_info) < 0) {
		if ((ret = mkdir(dir, S_IRWXU | S_IRWXG)) < 0) {
			return ret;
		}
	}
	
	/* create sub dirs */
	for (i = 0; i < astrodb_size(dirs); i++) {
		bzero(dir, PATH_SIZE);
		strncat(dir, location, PATH_SIZE);
		strncat(dir, dirs[i], PATH_SIZE);
		if (stat(dir, &stat_info) < 0) {
			if ((ret = mkdir(dir, S_IRWXU | S_IRWXG)) < 0) {
				return ret;
			}
		}
	}
	
	return ret;
}

/*! \fn astrodb_library* astrodb_open_library(char* remote, char* local)
 * \param local Local library repository location
 * \param remote Remote repository location
 * \returns A astrodb_library object or NULL on failure
 *
 * Initialises a CDS library structure on disk at location specified. 
 * 
 * This typically only needs to be called once per program.
 */

struct astrodb_library *astrodb_open_library(char *remote, char *local)
{
	struct astrodb_library *lib = NULL;
	int err;
	
	if (local == NULL)
		return NULL;
	
	lib = (struct astrodb_library *) malloc(sizeof(struct astrodb_library));
	if (lib == NULL)
		return NULL;
	bzero(lib, sizeof(struct astrodb_library));
	
	err = create_lib_local_dirs(local);
	if (err < 0) {
		astrodb_error("failed to create local lib dir %s %d\n", 
			local, err);
		goto err;
		
	}
	
	lib->local = strdup(local);
	if (lib->local == NULL)
		goto err;
	lib->remote = strdup(remote);
	if (lib->remote == NULL)
		goto err;
	return lib;
	
err:
	free(lib->local);
	free(lib->remote);
	free(lib);
	return NULL;
}


/*! \fn void astrodb_close_library(astrodb_library* lib)
 * \param lib Library to free
 *
 * Free's all library resources.
 */
void astrodb_close_library(struct astrodb_library * lib)
{
	free(lib->remote);
	free(lib->local);
	free(lib);
}

void astrodb_set_msg_level(enum astrodb_msg_level level)
{
	adb_msg_level = level;
}
