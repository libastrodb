/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood 
 */

#include <string.h>
#include <errno.h>

#include <libastrodb/db.h>
#include <libastrodb/table.h>
#include <libastrodb/adbstdio.h>

int table_is_field(struct astrodb_table * table, char *field)
{
	int i;
	
	for (i = 0; i < table->object_fields; i++) {
		if (!strcmp(table->idx[i].symbol, field)) {
			return 1;
		}
	}
	
	for (i = 0; i < table->alt_fields; i++) {
		if (!strcmp(table->alt_idx[i].pri.symbol, field)) {
			return 1;
		}
	}
	
	return 0;
}


/*! \fn int table_find_field(astrodb_table * table, char *field)
 * \brief Check if table contains field
 */
int table_find_field(struct astrodb_table *table, char *field)
{
	int i;
	
	for (i = 0; i < table->object_fields; i++) {
		if (!strcmp(field, table->idx[i].symbol))
			return i;
	}
	
	astrodb_error("field %s does not exist\n", field);
	return -EINVAL;
}

/*
 * Add an alternative field to the dataset. Move it's primary out
 * of the idx and into the alt idx.
 */
int table_add_alt_field(struct astrodb_table *table, char *field, int pri_idx)
{
	struct astrodb_dlist *desc = table->byte_description;
	struct astrodb_schema_object *sec = 
		&table->alt_idx[table->alt_fields].sec;
	
	/* find field in table */
	while (desc != NULL) {
		struct astrodb_table_column_info *byte = 
			(struct astrodb_table_column_info *) desc->data;
		
		if (!strncmp(field, byte->label, strlen(byte->label))) {
			
			sec->s_offset = table->object_size;
			sec->l_offset = byte->start;
			sec->l_size = byte->end - byte->start + 1;
			sec->type = table_get_ctype(byte->type);
			sec->s_size = table_get_csize(byte->type);
			strncpy(sec->name, byte->explanation, 32);
			strncpy(sec->units, byte->units, 16);
			strncpy(sec->symbol, byte->label, 8);
			table->alt_idx[table->alt_fields].insert = 
				table_get_alt_key_insert(sec->type);
		
			/* move primary field index and insert */
			memcpy(&table->alt_idx[table->alt_fields].pri, 
			       &table->idx[pri_idx], 
			       sizeof(struct astrodb_schema_object));
			astrodb_info("Object fields %d\n", table->object_fields);
			
			if (table->object_fields > pri_idx){
				memmove(&table->idx[pri_idx], 
					&table->idx[pri_idx+1], 
					sizeof(struct astrodb_schema_object) * 
					(table->object_fields - pri_idx));
			}
			
			table->object_fields--;			
			table->alt_fields++;
			return 1;
		}
		desc = desc->tail;
	}
	
	return 0;
}



static void dump_schema(struct astrodb_table *table)
{
	int i;
	
	astrodb_info("Table Schema:\n");
	astrodb_info("Index\tSymbol\tOffset\tSize\tLine\tLSize\tType\tUnits\t\n");
	
	for(i = 0; i < table->object_fields; i++) {
		astrodb_info("%d\t%s\t%d\t%d\t%d\t%d\t%d\t%s\n", i, 
			table->idx[i].symbol, 
			table->idx[i].s_offset, table->idx[i].s_size,
			table->idx[i].l_offset, table->idx[i].l_size,
			table->idx[i].type, table->idx[i].units);
	}
	
	astrodb_info("Alternative Column Sources:\n");
	for(i = 0; i < table->alt_fields; i++) {
		astrodb_info("Pri\t%s\t%d\t%d\t%d\t%d\t%d\t%s\n", 
			table->alt_idx[i].pri.symbol, 
			table->alt_idx[i].pri.s_offset, table->alt_idx[i].pri.s_size,
			table->alt_idx[i].pri.l_offset, table->alt_idx[i].pri.l_size,
			table->alt_idx[i].pri.type, table->alt_idx[i].pri.units);
		astrodb_info("Sec\t%s\t\t\t%d\t%d\t%d\t%s\n", 
			table->alt_idx[i].sec.symbol,
			table->alt_idx[i].sec.l_offset, table->alt_idx[i].sec.l_size,
			table->alt_idx[i].sec.type, table->alt_idx[i].sec.units);
	}
}

int table_order_index(struct astrodb_table *table)
{
	int i, j, lowest_src, src, group, fields = 0;
	struct astrodb_schema_object *idx, *idx_start;
	
	idx_start = idx = calloc(1, sizeof(struct astrodb_schema_object) * 
		table->object_fields);
	if (idx == NULL)
		return -ENOMEM;

	/* iterate through index, starting with lowest line pos first */
	for (j = 0; j < table->object_fields; j++) {
		lowest_src = 1024;
		
		for (i = 0; i < table->object_fields; i++) {
			if (lowest_src > table->idx[i].l_offset && 
				table->idx[i].s_size) {
				lowest_src = table->idx[i].l_offset;
				src = i;
			}
		}
		
		/* sort into group order highest group pos first */
		if (table->idx[src].g_offset) {
			group = 0;
			
			for (i = 0; i < table->object_fields; i++) {
				if (table->idx[i].g_offset == 
					table->idx[src].g_offset &&
				    	group < table->idx[i].g_posn && 
				    	table->idx[i].s_size) {
						group = table->idx[i].g_posn;
						src = i;
				    }
			}
		}
		
		/* do copy */
		memcpy(idx, &table->idx[src], 
			sizeof(struct astrodb_schema_object));
		table->idx[src].s_size = 0;
		idx++;
		fields++;
	}
	
	/* copy back to table */
	memcpy(table->idx, idx_start, 
		sizeof(struct astrodb_schema_object) * table->object_fields);
	free(idx_start);
	
	dump_schema(table);
	return 0;
}

/*! \fn int table_add_custom_struct_field(astrodb_table * table, astrodb_schema_object* idx)
 * \param table dataset
 * \param field Field name to add
 * \return 0 on success
 *
 * Add a custom struct field to a dataset for import. 
 */
int table_add_custom_struct_field(struct astrodb_table *table, 
					struct astrodb_schema_object *idx)
{
	struct astrodb_table_column_info *byte;
	struct astrodb_schema_object *didx;
	
	struct astrodb_dlist *desc = table->byte_description;
	
	while (desc != NULL) {
		byte = (struct astrodb_table_column_info *) desc->data;
		didx = &table->idx[table->object_fields];
			
		if (!strncmp(idx->symbol, byte->label, strlen(byte->label)) && 
		    !table_is_field(table, byte->label)) {
		    	
			memcpy(didx, idx, sizeof(struct astrodb_schema_object));
			didx->l_offset = byte->start;
			didx->l_size = byte->end - byte->start + 1;
			    
			if (didx->insert == NULL)
				didx->insert = table_get_key_insert(didx->type);
			
			/* make sure we are not over writting str data */
			if (didx->type == CT_STRING) {
				if (didx->s_size < didx->l_size) {
					astrodb_info("%s string too big at "
						"%d resized to %d\n", 
						didx->symbol,
						didx->l_size, 
						didx->s_size);
					didx->l_size = didx->s_size;
				}
			}
	    	
			table->object_fields++;
			table->object_size += idx->s_size;		
			return 0;
		}
		desc = desc->tail;
	}
	
	astrodb_error("field %s does not exist\n", idx->name);
	return -EINVAL;
}
