/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood
 */

#include <math.h>

#include <libastrodb/db.h>
#include <libastrodb/table.h>
#include <libastrodb/adbstdio.h>

#define D2R  (1.7453292519943295769e-2)  /* deg->radian */
#define R2D  (5.7295779513082320877e1)   /* radian->deg */

static void unit_vector(double ra, double dec,
	double *x, double *y, double *z)
{
	ra *= D2R;
	dec *= D2R;
	
	*x = cos(dec) * sin(ra);
	*y = sin(dec);
	*z = cos(dec) * cos(ra);
}

static void rotate_x(double *y, double *z, double a)
{	
	double _y, _z;
	
	_y = (*y * cos(a)) + (*z * -sin(a));
	_z = (*y * sin(a)) + (*z * cos(a));
	
	*y = _y;
	*z = _z;
}

static void rotate_y(double *x, double *z, double a)
{
	double _x, _z;
	
	_x = (*x * cos(a)) + (*z * sin(a));
	_z = (*x * -sin(a)) + (*z * cos(a));
	
	*x = _x;
	*z = _z;
}

/*! \fn int table_get_objects_mem(astrodb_table * table, astrodb_slist ** result)
 * \brief Get objects from memory based on posn
 */
int table_get_objects_mem(struct astrodb_table *table, 
				struct astrodb_slist **result)
{
	int count = 0, ra, dec, mag, offset;
	
	*result = NULL;
	for (ra = table->ra.clip_min; ra <= table->ra.clip_max; ra++) {
		
		for (dec = table->dec.clip_min; 
			dec <= table->dec.clip_max; dec++) {
			
			for (mag = table->mag.clip_max; 
				mag <= table->mag.clip_min; mag++) {
					
				offset = table_calc_offset(table, ra, dec, mag);
				
				if (*(table->objects + offset)) {
					*result = astrodb_slist_prepend(*result,
						 (char *) *(table->objects + offset));
					count += (table->status + offset)->size;
				}
			}
		}
	}
	return count;
}

/*! \fn int table_get_objects_cache(astrodb_table * table, astrodb_slist ** result, astrodb_progress progress)
 * \brief Get objects from memory based on posn
 */
int table_get_objects_cache(struct astrodb_table *table, 
				struct astrodb_slist **result)
{
	return 0;
}

/*! \fn int table_get_objects_raw(astrodb_table * table, astrodb_slist ** result, astrodb_progress progress)
 * \brief Get objects from memory based on posn
 */
int table_get_objects_raw(struct astrodb_table *table, 
				struct astrodb_slist **result)
{
	return 0;
}

/*! \fn int table_get_objects_online(astrodb_table * table, astrodb_slist ** result, astrodb_progress progress)
 * \brief Get objects from memory based on posn
 */
int table_get_objects_online(struct astrodb_table *table, 
				struct astrodb_slist **result)
{
	return 0;
}

static inline int is_tile_base_valid(struct astrodb_table *table, 
				int tile_ra, int tile_dec,
				double ra_angle, double dec_angle)
{
	double ra, dec, x, y, z;
	
	/* bottom left */
	ra = tile_ra * table->ra.stride_size;
	dec = (tile_dec * table->dec.stride_size) - 90.0;
	unit_vector(ra, dec, &x, &y, &z);
	rotate_y(&x, &z, ra_angle);
	rotate_x(&y, &z, dec_angle);
	if (z >= table->z_limit)
		return 1;

	/* bottom right */
	ra = (tile_ra + 1) * table->ra.stride_size;
	dec = (tile_dec * table->dec.stride_size) - 90.0;
	unit_vector(ra, dec, &x, &y, &z);
	rotate_y(&x, &z, ra_angle);
	rotate_x(&y, &z, dec_angle);
	if (z >= table->z_limit)
		return 1;

	/* top left */
	ra = tile_ra * table->ra.stride_size;
	dec = ((tile_dec + 1) * table->dec.stride_size) - 90.0;
	unit_vector(ra, dec, &x, &y, &z);
	rotate_y(&x, &z, ra_angle);
	rotate_x(&y, &z, dec_angle);
	if (z >= table->z_limit)
		return 1;
	
	/* top right */
	ra = (tile_ra + 1) * table->ra.stride_size;
	dec = ((tile_dec + 1) * table->dec.stride_size) - 90.0;
	unit_vector(ra, dec, &x, &y, &z);
	rotate_y(&x, &z, ra_angle);
	rotate_x(&y, &z, dec_angle);
	if (z >= table->z_limit)
		return 1;
		
	/* tile centre */
	ra = (tile_ra + 0.5) * table->ra.stride_size;
	dec = ((tile_dec + 0.5) * table->dec.stride_size) - 90.0;
	unit_vector(ra, dec, &x, &y, &z);
	rotate_y(&x, &z, ra_angle);
	rotate_x(&y, &z, dec_angle);
	if (z >= table->z_limit)
		return 1;
	
	/* not visible */
	return 0;
}

static int get_tile_base_objects(struct astrodb_table *table, 
				struct astrodb_slist **result, int ra, int dec)
{
	int mag, count = 0, offset;
	
	for (mag = table->mag.clip_max; 
		mag <= table->mag.clip_min; mag++) {
					
		offset = table_calc_offset(table, ra, dec, mag);
				
		if (*(table->objects + offset)) {
			*result = astrodb_slist_prepend(*result,
				 (char *) *(table->objects + offset));
			count += (table->status + offset)->size;
		}
	}
	return count;
}

/*! \fn int table_get_objects_memc(astrodb_table * table, astrodb_slist ** result)
 * \brief Get objects from memory based on posn
 */
int table_get_objects_memc(struct astrodb_table *table, 
				struct astrodb_slist **result)
{
	int count = 0, ra, dec, valid;
	double ra_angle, dec_angle;
	
	*result = NULL;
	
	table->z_limit = cos((table->fov * D2R) / 2.0) - 0.01;
	
	ra_angle = (360.0 - table->clip_centre_ra) * D2R;
	dec_angle = table->clip_centre_dec * D2R;
	
	for (ra = 0; ra < table->ra.stride; ra++) {
		for (dec = 0; dec < table->dec.stride; dec++) {
			valid = is_tile_base_valid(table, ra, dec, 
				ra_angle, dec_angle);
			if (valid)
				count += get_tile_base_objects(table, 
						result, ra, dec);
		}
	}
	
	return count;
}

/*! \fn int table_get_objects_cachec(astrodb_table * table, astrodb_slist ** result, astrodb_progress progress)
 * \brief Get objects from memory based on posn
 */
int table_get_objects_cachec(struct astrodb_table *table, 
				struct astrodb_slist **result)
{
	return 0;
}

/*! \fn int table_get_objects_rawc(astrodb_table * table, astrodb_slist ** result, astrodb_progress progress)
 * \brief Get objects from memory based on posn
 */
int table_get_objects_rawc(struct astrodb_table *table, 
				struct astrodb_slist **result)
{
	return 0;
}

/*! \fn int table_get_objects_onlinec(astrodb_table * table, astrodb_slist ** result, astrodb_progress progress)
 * \brief Get objects from memory based on posn
 */
int table_get_objects_onlinec(struct astrodb_table *table, 
				struct astrodb_slist **result)
{
	return 0;
}


/*! \fn int table_get_objects_cache_near(astrodb_table * table, astrodb_slist ** result)
 * \brief Get objects from memory based on distance
 */
int table_get_objects_cache_near(struct astrodb_table *table, 
					struct astrodb_slist **result)
{
	int count = 0, ra;
	
	*result = NULL;
	for (ra = table->ra.clip_min; ra <= table->ra.clip_max; ra++) {
		
		if (*(table->objects + ra)) {
			*result = astrodb_slist_prepend(*result, 
					(char *) *(table->objects + ra));
			count += (table->status + ra)->size;
		}
	}
	return count;
}
