/*
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  
 *  Copyright (C) 2008 Liam Girdwood
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <libastrodb/db.h>
#include <libastrodb/table.h>
#include <libastrodb/wget.h>
#include <libastrodb/gunzip.h>
#include <libastrodb/adbstdio.h>

/*
 * libastrodb interface version
 */
#define ADB_IDX_VERSION		2
#define FILE_NAME_SIZE		256

/*! \fn static void table_to_hdr (astrodb_table* table, astrodb_table_info* hdr)
 * \brief Convert a dataset to a dataset file header
 */
static void table_to_hdr(struct astrodb_table *table, 
			struct astrodb_table_index *hdr)
{
	hdr->ra_min = table->db->ra_min;
	hdr->ra_max = table->db->ra_max;
	hdr->dec_min = table->db->dec_min;
	hdr->dec_max = table->db->dec_max;
	hdr->mag_faint = table->db->mag_faint;
	hdr->mag_bright = table->db->mag_bright;
	hdr->object_size = table->object_size;
	hdr->object_fields = table->object_fields;
	hdr->table_size = table->table_size;
}

/*! \fn static void hdr_to_table (astrodb_table* table, astrodb_table_info* hdr)
 * \brief Convert a dataset file header to a dataset
 */
static void hdr_to_table(struct astrodb_table *table, 
			struct astrodb_table_index *hdr)
{
	table->db->ra_min = hdr->ra_min;
	table->db->ra_max = hdr->ra_max;
	table->db->dec_min = hdr->dec_min;
	table->db->dec_max = hdr->dec_max;
	table->db->mag_faint = hdr->mag_faint;
	table->db->mag_bright = hdr->mag_bright;
	table->object_size = hdr->object_size;
	table->object_fields = hdr->object_fields;
	table->table_size = hdr->table_size;
}


/*! \fn int table_cache_tiles(astrodb_table * table, astrodb_progress progress)
 * \brief Cache dataset object tiles to file
 */
int table_cache_tiles(struct astrodb_table *table)
{
	int count = 0, x;
	char file[FILE_NAME_SIZE];
	FILE *f;
	
	sprintf(file, "%s%s%s", table->db->local_path, table->file, ".dat");
	
	f = fopen(file, "w+");
	if (f == NULL) {
		astrodb_error("cant open table file %s for writing\n", file);
		return -EIO;	
	}
	
	for (x = 0; x < table->no_tiles; x++) {
		
		if (*(table->objects + x)) {
			
			struct astrodb_slist *object = *(table->objects + x);
			
			while (object) {
				fwrite(object->data, table->object_size, 1, f);				
				object = object->tail;
				count++;
			}	
		}
	}
	if (count != table->table_size)
		astrodb_error("wrote %d objects, expected %d\n", 
					count, table->table_size);
	fclose(f);
	return count;
}

/*! \fn int table_uncache_tiles(astrodb_table * table, astrodb_progress progress)
 * \brief Uncache dataset objects from file to memory
 */
int table_uncache_tiles(struct astrodb_table *table)
{
	int count = 0, size;
	char *object = NULL;
	char file[FILE_NAME_SIZE];
	FILE *f;
	
	if (table->object_size <= 0) {
		astrodb_error("invalid object size\n", table->object_size);
		return -EINVAL;	
	}
	
	sprintf(file, "%s%s%s", table->db->local_path, table->file, ".dat");
	
	f = fopen(file, "r");
	if (f == NULL) {
		astrodb_error("cant open table file %s for reading\n", file);
		return -EIO;	
	}
	
	astrodb_info("Uncaching %s object size is %d\n", file, 
		table->object_size);
	while (count < table->table_size) {
		object = calloc(1, table->object_size);
		size = fread(object, table->object_size, 1, f);
		
		if (size == 0) {
			if (count < table->table_size)
				astrodb_error("read %d objects, expected %d\n", 
					count, table->table_size);
			break;
		}
		table->object_insert(table, object);
		count++;

	}
	fclose(f);
	astrodb_info("Uncached and inserted %d objects\n", count);
	return count;
}

/*! \fn int table_save_hdr(astrodb_table * table)
 * Save a dataset header
 */
int table_save_hdr(struct astrodb_table *table)
{
	struct astrodb_table_index hdr;
	FILE *f;
	char file[128];
	
	hdr.catalog_magic = ADB_IDX_VERSION;
	
	bzero(&hdr, sizeof(struct astrodb_table_index));
	table_to_hdr(table, &hdr);
	sprintf(file, "%s%s%s", table->db->local_path, table->file, ".idx");
	
	f = fopen(file, "w+");
	if (f == NULL) {
		astrodb_error("cant open schema file %s for writing\n", file);
		return -EIO;		
	}	
	fwrite((char *) &hdr, sizeof(struct astrodb_table_index), 1, f);
	fwrite((char *) table->idx, sizeof(struct astrodb_schema_object), 
			table->object_fields, f);
	fclose(f);
	return 0;
}


/*! \fn int table_load_hdr(astrodb_table * table)
 * Load a dataset header from disk
 */
int table_load_hdr(struct astrodb_table *table)
{
	struct astrodb_table_index hdr;
	FILE *f;
	char file[128];
	
	sprintf(file, "%s%s%s", table->db->local_path, table->file, ".idx");
	f = fopen(file, "r");
	
	if (f == NULL) {
		astrodb_error("cant open schema file %s for reading\n", file);
		return -EIO;		
	}
	
	fread((char *) &hdr, sizeof(struct astrodb_table_index), 1, f);
	fread((char *) table->idx, sizeof(struct astrodb_schema_object), 
			table->object_fields, f);
	fclose(f);
	
	hdr_to_table(table, &hdr);
	return 0;
}

static int get_remote_spilt_dataset(struct astrodb_table *table)
{
	char ofile[1024], file[1024];
	struct dirent *dent;
	FILE *ofd;
	int res;
	
	sprintf(file, "%s%s", table->file, ".*.gz");
	
	res = wget_file(table->db, file);
	if (res < 0) {
		astrodb_error("cant get table %s\n", file);
		return -EIO;	
	}
	
		
	DIR *dir = opendir(table->db->local_path);
	if (dir == NULL) {
		astrodb_error("cant open directory %s\n", 
			table->db->local_path);
		return -EIO;	
	}
	
	while ((dent = readdir(dir)) != NULL) {
		if (!strncmp(table->file, dent->d_name, 
			strlen(table->file))) {
			
			/* unzip if we got it */
			res = gunzip_file(table->db, dent->d_name);
			if (res < 0) {
				astrodb_info(" res %d \n", res);
				return res;
			}
		}
	}
	closedir(dir);
	dir = opendir(table->db->local_path);
	if (dir == NULL) {
		astrodb_error("cant open directory %s\n", 
			table->db->local_path);
		return -EIO;
	}
	
	sprintf(ofile, "%s%s", table->db->local_path, table->file);
	ofd = fopen(ofile, "w");
	if (ofd == NULL) {
		astrodb_error("cant open file %s\n", ofd);
		return -EIO;
	}
	
	while ((dent = readdir(dir)) != NULL) {
		
		if (!strcmp(table->file, dent->d_name))
			continue;
		
		if (!strncmp(table->file, dent->d_name, strlen(table->file))) {
			char ifile[1024];
			char buff[4096];
			FILE *ifd;
			int size, sect, rem, i;
			
			sprintf(ifile, "%s%s", table->db->local_path, 
				dent->d_name);
			
			ifd = fopen(ifile, "r");
			if (ifd == NULL) {
				astrodb_error("cant open file %s for reading\n",
					ifd);
				return -EIO;
			}
			
			fseek(ifd, 0, SEEK_END);
			size = ftell(ifd);
			sect = size / 4096;
			rem = size % 4096;
			fseek(ifd, 0, SEEK_SET);
			
			/* db files into single dataset */
			for (i = 0; i < sect; i++) {
				fread(buff, 4096, 1, ifd);
				fwrite(buff, 4096, 1, ofd);
			}
			fread(buff, rem, 1, ifd);
			fwrite(buff, rem, 1, ofd);
			fclose(ifd);
		}
	}
	closedir(dir);
	fclose(ofd);

	return 0;
}

int table_get_remote_dataset(struct astrodb_table *table)
{
	int ret;
	struct stat stat_info;
	char file[1024];
	
	/* try dataset ReadMe filename first */
	ret = wget_file(table->db, table->file);
	if (ret == 0) {
		
		/* did we get it */
		sprintf(file, "%s%s", table->db->local_path, table->file);
		ret = stat(file, &stat_info);
		if (ret == 0)
			return 0;
	}
	
	/* now try ReadMe dataset name with a .gz extension */
	sprintf(file, "%s%s", table->file, ".gz");
	ret = wget_file(table->db, file);
	if (ret == 0) {
		
		/* unzip if we got it */
		ret = stat(file, &stat_info);
		if (ret == 0) {
			ret = gunzip_file(table->db, file);
			if (ret < 0) {
				astrodb_error("failed to gunzip %s\n", file);
				return ret;
			}
		
			return 0;
		}
	}
	
	/* now try broken up with .nn.gz extensions */
	ret = get_remote_spilt_dataset(table);
	if (ret == 0)
		return ret;
	
	/* give up ! */
	astrodb_error("could not download table %s\n", table->file);
	return -EIO;
}
