/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 *
 * Copyright (C) 2008 Liam Girdwood
 */


#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <stdio.h>

#include <libastrodb/astrodb.h>

static struct timeval start, end;

inline static void start_timer(void)
{
	gettimeofday(&start, NULL);
}

static void end_timer(int objects, int bytes)
{
	double usecs;

	gettimeofday(&end, NULL);
	usecs = (end.tv_sec * 1000000 + end.tv_usec) - 
		(start.tv_sec * 1000000 + start.tv_usec);

	if (bytes)
		printf ("Time(secs) %f : %9.0f objects per sec %9.0f bytes per"
			" secs\n", usecs / 1000000.0, 
			(1000000.0 / usecs) * objects,
	         	(1000000.0 / usecs) * bytes);
	else
		printf("Time(secs) %f : %9.0f objects per sec\n",
			usecs / 1000000.0, (1000000.0 / usecs) * objects);
}

/* 
 * Get all the objects in the dataset.
 */
static int get_all(struct astrodb_table * table)
{
	int count = 0;
	struct astrodb_slist *res = NULL;

	printf("Get all dataset objects\n");
	astrodb_table_unclip(table);
	count = astrodb_table_get_objects(table, &res, ADB_SMEM);
	printf("   Got %d objects\n", count);
	astrodb_table_put_objects(res);
	return 0;
}

int main(int argc, char *argv[])
{
	struct astrodb_db *db = NULL;
	struct astrodb_library *lib = NULL;
	struct astrodb_table *table = NULL;
	int table_size, object_size;

	printf("%s using libastrodb %s\n", argv[0], astrodb_get_version());
    
	/* set the remote db and initialise local repository/cache */
	lib = astrodb_open_library("ftp://cdsarc.u-strasbg.fr/pub/cats", "lnc-test");
    	if (lib == NULL) {
		printf("failed to open library\n");
		return -1;
	}

	/* create a dbalog, using class VII and dbalog 118 (NGC2000) */
	/* ra,dec,mag bounds are set here along with the 3d tile array size */
	db = astrodb_create_db(lib, "VII", "118", 0.0, 360.0, -90.0, 90.0, 
				15.0, -2.0, 0);
    	if (db == NULL) {
		printf("failed to create db\n");
		return -1;
	}

	/* use the first dataset in this example */
	table = astrodb_table_create(db, "ngc2000.dat", ADB_MEM | ADB_FILE );
   	if (table == NULL) {
		printf("failed to create table\n");
		return -1;
	}

	/* Import every field in the dataset. */
//	if (astrodb_table_add_custom_field(table, "*"))
		printf("failed to add *\n");

	/* Import the dataset from remote/local repo into memory/disk cache */
	start_timer();

	if (astrodb_table_open(table, 0, 0, 0) < 0) {
		printf("failed to open table\n");
		return -1;
	}

	table_size = astrodb_table_get_size(table);
	object_size = astrodb_table_get_row_size(table);
	end_timer(table_size, table_size * object_size);

	/* we can now perform operations on the dbalog data !!! */
	get_all(table);

	/* were done with the table */
	astrodb_table_close(table);

	/* were now done with db */
	astrodb_db_free(db);
	astrodb_close_library(lib);
	return 0;
}
