/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 *
 * Copyright (C) 2005 Liam Girdwood 
 */

#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <stdio.h>
#include <libastrodb/astrodb.h>

static struct timeval start, end;

inline static void start_timer(void)
{
    gettimeofday(&start, NULL);
}

static void end_timer(int objects, int bytes)
{
    double usecs;
    gettimeofday(&end, NULL);
    usecs = (end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec);

    if (bytes)
        printf
	    ("Time(secs) %f : %9.0f objects per sec %9.0f bytes per secs\n",
	     usecs / 1000000.0, (1000000.0 / usecs) * objects,
	     (1000000.0 / usecs) * bytes);
    else
        printf("Time(secs) %f : %9.0f objects per sec\n",
	       usecs / 1000000.0, (1000000.0 / usecs) * objects);
}

static void print_progress(float percent, char *msg, void *data)
{
    printf("Progess %f %s\n", percent, msg);
}

/*
 * Search for red shift > 0
 */
static int search(struct astrodb_table * table)
{
    struct astrodb_slist *res = NULL;
    struct astrodb_search *srch;
    int err;

    srch = astrodb_search_create(table);
    astrodb_search_add_comparator(srch, "z", ADB_COMP_GT, "0");
    astrodb_search_add_operator(srch, ADB_OP_OR);

    start_timer();
    if ((err = astrodb_search_get_results(srch, &res, ADB_SMEM)) < 0)
        printf("Search init failed %d\n", err);
    end_timer(astrodb_search_get_tests(srch), 0);
    printf("   Search got %d objects out of %d tests\n", astrodb_search_get_hits(srch), astrodb_search_get_tests(srch));
    astrodb_search_put_results(res);
    astrodb_search_free(srch);
    return 0;
}

void init_table(void *data, void *user)
{
    struct astrodb_table_info *i = (struct astrodb_table_info *) data;
    struct astrodb_db *db = (struct astrodb_db *) user;
    struct astrodb_table *table;
    int count;
    struct astrodb_slist *res = NULL;
    int table_size, object_size;

    printf("\nDset: \t%s\nrecords:\t%d\nrec len: \t%d\ntitle: \t%s\n",
	   i->name, i->records, i->length, i->title);

    if ((table = astrodb_table_create(db, i->name, ADB_MEM | ADB_FILE))) {
        start_timer();
  //      astrodb_table_add_custom_field(table, "*");
        if (astrodb_table_open(table, 0, 0, 0) < 0) {
            exit(-1);
        }
        table_size = astrodb_table_get_size(table);
        object_size = astrodb_table_get_row_size(table);
        end_timer(table_size, table_size * object_size);
        
        astrodb_table_unclip(table);
        count = astrodb_table_get_objects(table, &res, ADB_SMEM);
        printf("Got %d objects\n", count);
        astrodb_table_put_objects(res);

        search(table);
        astrodb_table_close(table);
    }
}

int main(int argc, char *argv[])
{
    struct astrodb_db *db = NULL;
//  astrodb_progress progress = print_progress;
    struct astrodb_library *lib = NULL;
    struct astrodb_dlist *datasets;

    printf("%s using libastrodb %s\n", argv[0], astrodb_get_version());
    
    /* set the remote db and initialise local repository/cache */
    if ((lib = astrodb_open_library("ftp://cdsarc.u-strasbg.fr/pub/cats", "lnc-test")) == NULL) {
        exit(-1);
    }

    /* create a dbalog, using class V and dbalog 86 */
    /* ra,dec,mag bounds are set here along with the 3d tile array size */
    if ((db = astrodb_create_db(lib, "VII", "110A", 0.0, 360.0, -90.0, 90.0, 15.0, -2.0, 0)) == NULL) {
        exit(-1);
    }

    datasets = astrodb_db_get_tables(db);
    astrodb_dlist_foreach(datasets, init_table, db);

    /* were now done with dbalog */
    astrodb_db_free(db);
    astrodb_close_library(lib);
    return 0;
}
