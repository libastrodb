/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 *
 * Copyright (C) 2005 Liam Girdwood 
 */

#define _GNU_SOURCE		/* for strtof and NAN */

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <libastrodb/astrodb.h>

struct hyperleda_object {
	struct astrodb_object d;
	float position_angle;
	float axis_ratio;
	char MType[4];
	char OType[1];
	char other_name[15];
};

static int pa_insert(void *dest, void *src)
{
	char *ptr;
	
	*(float *) dest = strtof(src, &ptr);
	if (src == ptr) {
		*(float*) dest = FP_NAN;
		return -1;
	}
	if (*(float *) dest == (float) 999.0)
		*(float*) dest = FP_NAN;

	return 0;
}

static int size_insert(void *dest, void *src)
{
	char *ptr;
	
	*(float *) dest = strtof(src, &ptr);
	if (src == ptr) {
		*(float*) dest = FP_NAN;
		return -1;
	}

	if (*(float *) dest == (float) 9.99)
		*(float*) dest = FP_NAN;
	return 0;
}

static int otype_insert(void *dest, void *src)
{
	char *type = src;
	
	if (*type == 'M')
		*(char*)dest = 'M';
	else if (*type == 'G') {
		if (*(type + 1) == 'M')
			*(char*)dest = 'X';
		else
			*(char*)dest = 'G';
	}

	return 0;
}

static struct astrodb_schema_object hyperleda_fields[] = {
	astrodb_member("Name", "ANames", struct hyperleda_object, other_name,
		   CT_STRING, "", 0, NULL),
	astrodb_member("ID", "PGC", struct hyperleda_object, d.id,
		   CT_INT, "", 0, NULL),
	astrodb_gmember("RA Hours", "RAh", struct hyperleda_object, d.posn_size.ra, 
		    CT_DOUBLE_HMS_HRS, "hours", 2, NULL),
	astrodb_gmember("RA Minutes", "RAm", struct hyperleda_object, d.posn_size.ra, 
		    CT_DOUBLE_HMS_MINS, "minutes", 1, NULL),
	astrodb_gmember("RA Seconds", "RAs", struct hyperleda_object, d.posn_size.ra, 
		    CT_DOUBLE_HMS_SECS, "seconds", 0, NULL),
	astrodb_gmember("DEC Degrees", "DEd", struct hyperleda_object, d.posn_size.dec, 
		    CT_DOUBLE_DMS_DEGS, "degrees", 3, NULL),
	astrodb_gmember("DEC Minutes", "DEm", struct hyperleda_object, d.posn_size.dec, 
		    CT_DOUBLE_DMS_MINS, "minutes", 2, NULL),
	astrodb_gmember("DEC Seconds", "DEs", struct hyperleda_object, d.posn_size.dec, 
		    CT_DOUBLE_DMS_SECS, "seconds", 1, NULL),
	astrodb_gmember("DEC sign", "DE-", struct hyperleda_object, d.posn_size.dec, 
		    CT_SIGN, "", 0, NULL),
	astrodb_member("Type", "MType", struct hyperleda_object, MType, 
		   CT_STRING, "", 0, NULL),
	astrodb_member("OType", "OType", struct hyperleda_object, OType,
		   CT_STRING, "", 0, otype_insert),
	astrodb_member("Diameter", "logD25", struct hyperleda_object,  d.posn_size.size, 
		   CT_FLOAT, "0.1amin", 0, size_insert),
	astrodb_member("Axis Ratio", "logR25", struct hyperleda_object, axis_ratio, 
		   CT_FLOAT, "0.1amin", 0, size_insert),
	astrodb_member("Position Angle", "PA", struct hyperleda_object, position_angle, 
		   CT_FLOAT, "deg", 0, pa_insert),
};

int main (int argc, char* argv[])
{ 
	struct astrodb_library *lib;
	struct astrodb_db *db;
	struct astrodb_table *table;
	
	/* set the remote db and initialise local repository/cache */
	lib = astrodb_open_library("ftp://cdsarc.u-strasbg.fr/pub/cats", "lnc-test");
	if (lib == NULL) {
		printf("failed to open library\n");
		return -1;
	}

	/* create a dbalog */
	db = astrodb_create_db(lib, "VII", "237", 
			0.0, 360.0, -90.0, 90.0, 15.0, -2.0, 0);
	if (db == NULL) {
		printf("failed to create db\n");
		return -1;
	}

	table = astrodb_table_create(db, "pgc", ADB_MEM | ADB_FILE );
    	if (table == NULL) {
		printf("failed to create table\n");
		return -1;
	}

	if (astrodb_table_register_schema(table, hyperleda_fields, 
		astrodb_size(hyperleda_fields), sizeof(struct hyperleda_object)) < 0)
		printf("%s: failed to register object type\n", __func__);

	/* We want to quickly search the dataset based on object ID and HD number */
	if (astrodb_table_hash_key(table, "PGC"))
		printf("%s: failed to hash on ID\n", __func__);

	/* Import the dataset from remote/local repo into memory/disk cache */   
	if (astrodb_table_open(table, 20, 10, 1) < 0) {
		printf("failed to open table\n");
		return -1;
	}

	/* were done with the dataset */
	astrodb_table_close(table);

	/* were now done with dbalog */
	astrodb_db_free(db);
		
	astrodb_close_library(lib);
	return 0;
}
