/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 *
 * Copyright (C) 2008 Liam Girdwood  
 */

#define _GNU_SOURCE		/* for NAN */

#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <stdio.h>
#include <math.h>

#include <libastrodb/astrodb.h>

static struct timeval start, end;

inline static void start_timer(void)
{
	gettimeofday(&start, NULL);
}

static void end_timer(int objects, int bytes)
{
	double usecs;
    
	gettimeofday(&end, NULL);
	usecs = (end.tv_sec * 1000000 + end.tv_usec) - 
		(start.tv_sec * 1000000 + start.tv_usec);

	if (bytes)
		printf ("Time(secs) %f : %9.0f objects per sec %9.0f bytes"
		" per secs\n", usecs / 1000000.0, 
		(1000000.0 / usecs) * objects, (1000000.0 / usecs) * bytes);
	else
		printf("Time(secs) %f : %9.0f objects per sec\n",
			usecs / 1000000.0, (1000000.0 / usecs) * objects);
}

/*
 * Search for all stars with:-
 *
 * ((pmRA < 0.2 && pmRA > 0.05) ||
 * (pmDE < 0.2 && pmDE > 0.05)) ||
 * (RV < 40 && RV > 25)
 *
 * Uses Reverse Polish Notation to define search parameters
 */
static int search1(struct astrodb_table * table)
{
	struct astrodb_slist *res = NULL;
	struct astrodb_search *srch;
	int err;

	printf("Searching for high pm or RV objects\n");
	
	srch = astrodb_search_create(table);
	if (astrodb_search_add_comparator(srch, "pmRA", ADB_COMP_LT, "0.2"))
		printf("failed to add comp pmRA\n");
	if (astrodb_search_add_comparator(srch, "pmRA", ADB_COMP_GT, "0.05"))
		printf("failed to add comp pmRA\n");
	if (astrodb_search_add_operator(srch, ADB_OP_AND))
		printf("failed to add op and\n");
	if (astrodb_search_add_comparator(srch, "pmDE", ADB_COMP_LT, "0.2"))
		printf("failed to add comp pmDE\n");
	if (astrodb_search_add_comparator(srch, "pmDE", ADB_COMP_GT, "0.05"))
		printf("failed to add comp pmDE\n");
	if (astrodb_search_add_operator(srch, ADB_OP_AND))
		printf("failed to add op and\n");
	if (astrodb_search_add_comparator(srch, "RV", ADB_COMP_LT, "40"))
		printf("failed to add comp RV\n");
	if (astrodb_search_add_comparator(srch, "RV", ADB_COMP_GT, "25"))
		printf("failed to add comp RV\n");
	if (astrodb_search_add_operator(srch, ADB_OP_AND))
		printf("failed to add op and\n");
	if (astrodb_search_add_operator(srch, ADB_OP_OR))
		printf("failed to add op or\n");

	start_timer();
	if ((err = astrodb_search_get_results(srch, &res, ADB_SMEM)) < 0) {
		printf("Search init failed %d\n", err);
		astrodb_search_free(srch);
		return err;
	}
	end_timer(astrodb_search_get_tests(srch), 0);

	printf("   Search got %d objects out of %d tests\n", 
		astrodb_search_get_hits(srch), astrodb_search_get_tests(srch));
	astrodb_search_put_results(res);
	astrodb_search_free(srch);
	return 0;
}


/*
 * Search for all G type stars
 * Uses Wildcard "G5*" to match with Sp
 */
static int search2(struct astrodb_table * table)
{
	struct astrodb_slist *res = NULL;
	struct astrodb_search *srch;
	int err;

	srch = astrodb_search_create(table);
	if (astrodb_search_add_comparator(srch, "Sp", ADB_COMP_LT, "G5*"))
		printf("failed to add comp G5*\n");
	if (astrodb_search_add_operator(srch, ADB_OP_OR))
		printf("failed to add op or\n");

	printf("Searching for G5 class objects\n");
	start_timer();
	err = astrodb_search_get_results(srch, &res, ADB_SMEM);
	if (err < 0) {
		printf("Search init failed %d\n", err);
		astrodb_search_free(srch);
		return err;
	}
	
	end_timer(astrodb_search_get_tests(srch), 0);
	printf("   Search got %d objects out of %d tests\n", 
		astrodb_search_get_hits(srch), 
		astrodb_search_get_tests(srch));
	astrodb_search_put_results(res);
	astrodb_search_free(srch);
	return 0;
}

struct s_data {
	int id_offset;
	int mag_offset;
	int ra_offset;
	int dec_offset;
};

void search3_print(void *object, void *data)
{
	struct s_data *s = (struct s_data *) data;

	printf("Obj: %s RA: %f DEC: %f Mag %f\n",
		(char *) (object + s->id_offset),
		*((double *) (object + s->ra_offset)),
		*((double *) (object + s->dec_offset)),
		*((float *) (object + s->mag_offset)));
}

/*
 * Search for all M1 type stars
 * Uses Wildcard "M1" to match with Sp
 */
static int search3(struct astrodb_table * table)
{
	struct astrodb_slist *res = NULL;
	struct astrodb_search *srch;
	int err;
	struct s_data s;

	srch = astrodb_search_create(table);
	if (astrodb_search_add_comparator(srch, "Sp", ADB_COMP_LT, "M1*"))
		printf("failed to add comp M1*\n");
	if (astrodb_search_add_operator(srch, ADB_OP_OR))
		printf("failed to add op or\n");

	printf("Searching for M1 class objects\n");
	start_timer();
	err = astrodb_search_get_results(srch, &res, ADB_SMEM);
	if (err < 0) {
		printf("Search init failed %d\n", err);
		astrodb_search_free(srch);
		return err;
	}
	end_timer(astrodb_search_get_tests(srch), 0);
	printf("   Search got %d objects out of %d tests\n", 
		astrodb_search_get_hits(srch), 
		astrodb_search_get_tests(srch));

	s.id_offset = astrodb_table_get_column_offset(table, "ID");
	s.mag_offset = astrodb_table_get_column_offset(table, "Vmag");
	s.ra_offset = astrodb_table_get_column_offset(table, "RA");
	s.dec_offset = astrodb_table_get_column_offset(table, "DEC");

	astrodb_table_for_search_results_do(res, search3_print, &s);
	astrodb_search_put_results(res);
	astrodb_search_free(srch);
	return 0;
}

/* 
 * Get all the objects in the dataset.
 */
static int get1(struct astrodb_table * table)
{
	int count = 0;
	struct astrodb_slist *res = NULL;

	printf("Get all dataset objects\n");
	astrodb_table_unclip(table);
	count = astrodb_table_get_objects(table, &res, ADB_SMEM);
	printf("   Got %d objects\n", count);
	astrodb_table_put_objects(res);
	return 0;
}

/* 
 * Get all the objects brighter than mag 3 in the dataset.
 */
static int get2(struct astrodb_table * table)
{
	int count = 0;
	struct astrodb_slist *res = NULL;

	printf("Get all objects < mag 3\n");

	/* we clip the dbalog in terms of RA, DEC and mag, this is much faster
	 * than searching, but suffers from boundary overlaps i.e.
	 * we may get some objects fainter than mag 3 depending on the mag clip
	 * boundary. This is not a problem for rendering sky views.
	 */
	astrodb_table_clip_on_position(table, 0, -90, 360, 90, 3, -2);
	count = astrodb_table_get_objects(table, &res, ADB_SMEM);
	printf("   Got %d objects\n", count);
	astrodb_table_put_objects(res);
	astrodb_table_unclip(table);
	return 0;
}

/* 
 * Get all the objects brighter than mag 3 in the dataset.
 */
static int get3(struct astrodb_table * table)
{
	int count = 0;
	struct astrodb_slist *res = NULL;
	struct s_data s;
    
	printf("Get all objects < mag 3, in radius 30 deg around 0,0\n");

	/* we clip the dbalog in terms of RA, DEC and mag, this is much faster
	 * than searching, but suffers from boundary overlaps i.e.
	 * we may get some objects fainter than mag 3 depending on the mag clip
	 * boundary. This is not a problem for rendering sky views.
	 */
	astrodb_table_clip_on_fov(table, 60, 0, 10, 5, -2);
	count = astrodb_table_get_objects(table, &res, ADB_SMEM);
	printf("   Got %d objects\n", count);
    
	s.id_offset = astrodb_table_get_column_offset(table, "ID");
	s.mag_offset = astrodb_table_get_column_offset(table, "Vmag");
	s.ra_offset = astrodb_table_get_column_offset(table, "RA");
	s.dec_offset = astrodb_table_get_column_offset(table, "DEC");
	astrodb_table_for_search_results_do(res, search3_print, &s);
    
	astrodb_table_put_objects(res);
	astrodb_table_unclip(table);
	return 0;
}

int main(int argc, char *argv[])
{
	struct astrodb_db *db = NULL;
	struct astrodb_library *lib = NULL;
	struct astrodb_table *table = NULL;
	int table_size, object_size;

	printf("%s using libastrodb %s\n", argv[0], astrodb_get_version());
    
	/* set the remote db and initialise local repository/cache */
	lib = astrodb_open_library("ftp://cdsarc.u-strasbg.fr/pub/cats", 
					"lnc-test");
	if (lib == NULL) {
		printf("failed to open library\n");
		return -1;
	}

	/* create a dbalog, using class V and dbalog 109 (Skymap2000) */
	/* ra,dec,mag bounds are set here along with the 3d tile array size */
	db = astrodb_create_db(lib, "V", "109", 0.0, 360.0, -90.0, 90.0, 
				15.0, -2.0, 0);
	if (db == NULL) {
		printf("failed to create db\n");
		return -1;
	}

	/* use the first dataset in this example */
	table = astrodb_table_create(db, "sky2kv4", ADB_MEM | ADB_FILE );
    	if (table == NULL) {
		printf("failed to create table\n");
		return -1;
	}
#if 0
	/* RA, DEC, Vmag and ID/Name are imported by default 
	 * Add a couple of custom fields that we are interested in
	 */
	if (astrodb_table_add_custom_field(table, "pmRA"))
		printf("failed to add pmRA\n");
	if (astrodb_table_add_custom_field(table, "pmDE"))
		printf("failed to add pmDE\n");
	if (astrodb_table_add_custom_field(table, "RV"))
		printf("failed to add RV\n");
	if (astrodb_table_add_custom_field(table, "Sp"))
		printf("failed to add Sp\n");
	if (astrodb_table_add_custom_field(table, "Vder"))
		printf("failed to add Vder\n");

	/* Vmag is blank in some records in the dataset, so we use can Vder
	 * as an alternate field.
	 */
	if (astrodb_table_alt_column(table, "Vmag", "Vder", 0))
		printf("failed to add alt index\n");

	/* Import every field in the dataset. */
	if (astrodb_table_add_custom_field(table, "*"))
		printf("failed to add *\n");
	if (astrodb_table_alt_column(table, "Vmag", "Vder", 0))
		printf("failed to add alt index\n");
#endif

	/* Import the dataset from remote/local repo into memory/disk cache */
	start_timer();
	if (astrodb_table_open(table, 0, 0, 0) < 0) {
		printf("failed to open table\n");
		return -1;
	}
    
	table_size = astrodb_table_get_size(table);
	object_size = astrodb_table_get_row_size(table);
	end_timer(table_size, table_size * object_size);

	/* we can now perform operations on the dbalog data !!! */
	get1(table);
	get2(table);
	search1(table);
	search2(table);
	search3(table);
	get3(table);

	/* were done with the dataset */
	astrodb_table_close(table);

	/* were now done with dbalog */
	astrodb_db_free(db);
	astrodb_close_library(lib);
	return 0;
}
